from django import forms
from django.db import connection

def get_nik_pasien():
    stmt = '''
    SELECT nik FROM pasien;
    '''
    kode = tuple()
    with connection.cursor() as cursor:
        cursor.execute(stmt)
        kode = cursor.fetchall()
    return [('NIK Pasien', 'NIK Pasien')] + [(a[0], a[0]) for a in kode]

class AppointmentDokterForm(forms.Form):

    nik_attrs = {
        'class' : 'form-control appointment',
        'placeholder' : 'NIK Pasien'
    }

    email_attrs = {
        'class' : 'form-control appointment',
        'disabled' : True,
    }

    kode_faskes_attrs = {
        'class' : 'form-control appointment',
        'disabled' : True,
    }

    tanggal_attrs = {
        'class' : 'form-control appointment',
        'disabled' : True,
    }

    shift_attrs = {
        'class' : 'form-control',
        'disabled' : True,
    }

    def __init__(self, *args, **kwargs):
        super(AppointmentDokterForm, self).__init__(*args, **kwargs)
        self.fields['nik'].choices = get_nik_pasien()

    nik = forms.ChoiceField(choices=get_nik_pasien(), widget=forms.Select(attrs=nik_attrs))
    email = forms.CharField(widget=forms.TextInput(attrs=email_attrs))
    kodeFaskes = forms.CharField(widget=forms.TextInput(attrs=kode_faskes_attrs))
    shift = forms.CharField(widget=forms.TextInput(attrs=shift_attrs))
    tanggal = forms.CharField(widget=forms.TextInput(attrs=tanggal_attrs))

class UpdateAppointmentDokterForm(forms.Form):

    nik_attrs = {
        'class' : 'form-control appointment',
        'disabled' : True,
    }

    email_attrs = {
        'class' : 'form-control appointment',
        'disabled' : True,
    }

    kode_faskes_attrs = {
        'class' : 'form-control appointment',
        'disabled' : True,
    }

    tanggal_attrs = {
        'class' : 'form-control appointment',
        'disabled' : True,
    }

    shift_attrs = {
        'class' : 'form-control',
        'disabled' : True,
    }

    rekomendasi_attrs = {
        'class' : 'form-control',
        'requried' : True,
        'placeholder' : 'Rekomendasi',
        'maxlength': 500,
    }

    nik = forms.CharField(widget=forms.TextInput(attrs=nik_attrs))
    email = forms.CharField(widget=forms.TextInput(attrs=email_attrs))
    kodeFaskes = forms.CharField(widget=forms.TextInput(attrs=kode_faskes_attrs))
    shift = forms.CharField(widget=forms.TextInput(attrs=shift_attrs))
    tanggal = forms.CharField(widget=forms.TextInput(attrs=tanggal_attrs))
    rekomendasi = forms.CharField(widget=forms.TextInput(attrs=rekomendasi_attrs))

class CreateRuanganRSForm(forms.Form):

    kode_rs_attrs = {
        'class' : 'form-control Bed RS',
        'required' : True,
        'placeholder' : 'Kode Rumah Sakit',
        'id' : 'koders'
    }

    kode_ruangan_attrs = {
        'class' : 'form-control Ruangan RS',
        'disabled' : True,
        'placeholder' : 'Kode Ruangan',
        'id' : 'koderuangan'
    }

    tipe_attrs = {
        'class' : 'form-control Ruangan RS',
        'required' : True,
        'placeholder' : 'Tipe',
        'maxlength': 10,
    }

    harga_attrs = {
        'class' : 'form-control Ruangan RS',
        'required' : True,
        'placeholder' : 'Harga',
    }

    def get_kode_rs():
        stmt = '''
        SELECT kode_faskes FROM rumah_sakit;
        '''
        kode = tuple()
        with connection.cursor() as cursor:
            cursor.execute(stmt)
            kode = cursor.fetchall()
        return [('Kode RS', 'Kode RS')] + [(a[0], a[0]) for a in kode]

    koders = forms.ChoiceField(choices=get_kode_rs(), widget=forms.Select(attrs=kode_rs_attrs))
    kode_ruangan = forms.CharField(widget=forms.TextInput(attrs=kode_ruangan_attrs))
    tipe = forms.CharField(widget=forms.TextInput(attrs=tipe_attrs))
    harga = forms.CharField(widget=forms.TextInput(attrs=harga_attrs))

class UpdateRuanganRSForm(forms.Form):

    kode_rs_attrs = {
        'class' : 'form-control Ruangan RS',
        'disabled' : True
    }

    kode_ruangan_attrs = {
        'class' : 'form-control Ruangan RS',
        'disabled' : True
    }

    tipe_attrs = {
        'class' : 'form-control Ruangan RS',
        'required' : True,
        'placeholder' : 'Tipe',
    }

    harga_attrs = {
        'class' : 'form-control Ruangan RS',
        'required' : True,
        'placeholder' : 'Harga',
    }

    kodeRS = forms.CharField(widget=forms.TextInput(attrs=kode_rs_attrs))
    kodeRuangan = forms.CharField(widget=forms.TextInput(attrs=kode_ruangan_attrs))
    tipe = forms.CharField(widget=forms.TextInput(attrs=tipe_attrs))
    harga = forms.CharField(widget=forms.TextInput(attrs=harga_attrs))

class CreateBedRSForm(forms.Form):

    kode_rs_attrs = {
        'class' : 'form-control Bed RS',
        'required' : True,
        'placeholder' : 'Kode Rumah Sakit',
        'id' : 'kodeRS'
    }

    kode_ruangan_attrs = {
        'class' : 'form-control Bed RS',
        'required' : True,
        'placeholder' : 'Kode Ruangan',
        'id' : 'kodeRuangan'
    }

    kode_bed_attrs = {
        'class' : 'form-control Bed RS',
        'disabled' : True,
        'placeholder' : 'Kode Bed',
        'id' : 'kodeBed'
    }

    def get_kode_rs():
        stmt = '''
        SELECT kode_faskes FROM rumah_sakit;
        '''
        kode_rs = tuple()
        with connection.cursor() as cursor:
            cursor.execute(stmt)
            kode_rs = cursor.fetchall()
        return [('Kode RS', 'Kode RS')] + [(kode[0], kode[0]) for kode in kode_rs]

    def get_kode_ruangan():
        stmt = '''
        SELECT koderuangan FROM ruangan_rs;
        '''
        kode_ruangan = tuple()
        with connection.cursor() as cursor:
            cursor.execute(stmt)
            kode_ruangan = cursor.fetchall()
        
        return [('Kode Ruangan', 'Kode Ruangan')] + [(kode[0], kode[0]) for kode in kode_ruangan]

    kode_rs = forms.ChoiceField(choices=get_kode_rs(), widget=forms.Select(attrs=kode_rs_attrs))
    kode_ruangan = forms.ChoiceField(choices=get_kode_ruangan(), widget=forms.Select(attrs=kode_ruangan_attrs))
    kode_bed = forms.CharField(widget=forms.TextInput(attrs=kode_bed_attrs))

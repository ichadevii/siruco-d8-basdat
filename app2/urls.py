from django.urls import re_path, path
from .views import *

app_name = 'app2'

urlpatterns = [
    path('listJadwalDokter/', listJadwalDokter, name='listJadwalDokter'),
    path('buatJadwalDokter/', buatJadwalDokter, name='buatJadwalDokter'),
    path('tambahJadwalDokter/<str:kodeFaskes>/<str:shift>/<str:tanggal>', tambahJadwalDokter, name='tambahJadwalDokter'),
    path('buatAppointmentDenganDokter/', buatAppointmentDenganDokter, name='buatAppointmentDenganDokter'),
    path('tambahAppointmentDenganDokter/<str:noSTR>/<str:email>/<str:kodeFaskes>/<str:shift>/<str:tanggal>/', tambahAppointmentDenganDokter, name='tambahAppointmentDenganDokter'),
    path('listAppointmentDenganDokter/', listAppointmentDenganDokter, name='listAppointmentDenganDokter'),
    path('updateAppointmentDenganDokter/<str:nik>/<str:noSTR>/<str:email>/<str:kodeFaskes>/<str:shift>/<str:tanggal>/', updateAppointmentDenganDokter, name='updateAppointmentDenganDokter'),
    path('deleteAppointmentDenganDokter/<str:nik>/<str:noSTR>/<str:email>/<str:kodeFaskes>/<str:shift>/<str:tanggal>/', deleteAppointmentDenganDokter, name='deleteAppointmentDenganDokter'),
    path('buatRuanganRS/', buatRuanganRS, name='buatRuanganRS'),
    path('dataKodeRuangan/', dataKodeRuangan, name='dataKodeRuangan'),
    path('listRuanganRS/', listRuanganRS, name='listRuanganRS'),
    path('updateRuanganRS/<str:kodeRS>/<str:kodeRuangan>/', updateRuanganRS, name='updateRuanganRS'),
    path('buatBedRS/', buatBedRS, name='buatBedRS'),
    path('jsonKodeRuangan/', jsonKodeRuangan, name='jsonKodeRuangan'),
    path('listBedRS/', listBedRS, name='listBedRS'),
    path('deleteBedRS/<str:kodeRS>/<str:kodeRuangan>/<str:kodeBed>/', deleteBedRS, name='deleteBedRS'),
]
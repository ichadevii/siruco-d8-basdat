from django.shortcuts import render, redirect
from django.db import connection
from django.http import JsonResponse
from django.shortcuts import render
from .forms import *
from datetime import datetime
from django import forms

url_dashboard = 'app1:dashboard'
url_login = 'app1:login'
url_listPasien = 'app1:listPasien'

def set_username(request, response):
    response['is_authenticated'] = request.session.get('is_authenticated')
    response['username'] = request.session.get('username')
    response['peran'] = request.session.get('peran')

def listJadwalDokter(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)
    stmt1 = '''
    SELECT * FROM jadwal_dokter WHERE username='{0}'
    '''.format(response['username'])
    stmt2 = '''
    SELECT * FROM jadwal_dokter
    '''
    if(response['peran'] == 'dokter'):
        try:
            jadwal = tuple()
            with connection.cursor() as cursor:
                cursor.execute(stmt1)
                jadwal = cursor.fetchall()
            newJadwal = [(a[0], a[1], a[2], a[3], a[4].strftime('%Y-%m-%d'), a[5]) for a in jadwal]
            response['jadwal'] = newJadwal
        except Exception as e:
            e = str(e)
            print(e)
    if(response["peran"] == 'pengguna_publik' or response["peran"] == 'admin_satgas'):
        try:
            jadwal = tuple()
            with connection.cursor() as cursor:
                cursor.execute(stmt2)
                jadwal = cursor.fetchall()
            newJadwal = [(a[0], a[1], a[2], a[3], a[4].strftime('%Y-%m-%d'), a[5]) for a in jadwal]
            response['jadwal'] = newJadwal
        except Exception as e:
            e = str(e)
            print(e)
    return render(request, 'app2/listJadwalDokter.html', response)

def buatJadwalDokter(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)
    stmt1 = '''
    SELECT kode_faskes, shift, tanggal FROM jadwal EXCEPT SELECT J.kode_faskes, J.shift, J.tanggal FROM jadwal J, jadwal_dokter JD WHERE J.kode_faskes = JD.kode_faskes AND J.shift = JD.shift AND J.tanggal = JD.tanggal AND JD.username = '{0}';
    '''.format(response['username'])
    if(response['peran'] == 'dokter'):
        try:
            jadwal = tuple()
            with connection.cursor() as cursor:
                cursor.execute(stmt1)
                jadwal = cursor.fetchall()
            newJadwal = [(a[0], a[1], a[2].strftime('%Y-%m-%d')) for a in jadwal]
            response['jadwal'] = newJadwal
        except Exception as e:
            e = str(e)
            print(e)
    return render(request, 'app2/buatJadwalDokter.html', response)

def tambahJadwalDokter(request, kodeFaskes, shift, tanggal):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)
    tanggal = datetime.strptime(tanggal, '%Y-%m-%d').date()
    jmlpasien = 0
    stmt1 = '''
    SELECT noSTR from dokter where username = '{0}'
    '''.format(response['username'])
    stmt2 = '''
    INSERT INTO jadwal_dokter(nostr, username, kode_faskes, shift, tanggal, jmlpasien) VALUES (%s, %s, %s, %s, %s, %s);
    '''
    if(response['peran'] == 'dokter'):
        try:
            with connection.cursor() as cursor:
                cursor.execute(stmt1)
                noSTR = cursor.fetchone()[0]
            with connection.cursor() as cursor:
                cursor.execute(stmt2, [noSTR, response['username'], kodeFaskes, shift, tanggal, jmlpasien])
                connection.commit()
            return redirect('app2:buatJadwalDokter')
        except Exception as e:
            e = str(e)
            print(e)
    return render(request, 'app2/buatJadwalDokter.html', response)

def buatAppointmentDenganDokter(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)
    stmt1 = '''
    SELECT * FROM jadwal_dokter;
    '''
    if(response['peran'] == 'pengguna_publik' or response['peran'] == 'admin_satgas'):
        try:
            jadwal = tuple()
            with connection.cursor() as cursor:
                cursor.execute(stmt1)
                jadwal = cursor.fetchall()
            newJadwal = [(a[0], a[1], a[2], a[3], a[4].strftime('%Y-%m-%d'), a[5]) for a in jadwal]
            response['jadwal'] = newJadwal
        except Exception as e:
            e = str(e)
            print(e)
    return render(request, 'app2/buatAppointmentDenganDokter.html', response)

def tambahAppointmentDenganDokter(request, noSTR, email, kodeFaskes, shift, tanggal):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)
    tanggal = datetime.strptime(tanggal, '%Y-%m-%d').date()
    if (request.method == 'POST'):
        isian = request.POST.copy()
        isian['email'] = email
        isian['kodeFaskes'] = kodeFaskes
        isian['shift'] = shift
        isian['tanggal'] = tanggal
        form = AppointmentDokterForm(isian)
        if(form.is_valid()):
            stmt1 = '''
            INSERT INTO memeriksa(nik_pasien, nostr, username_dokter, kode_faskes, praktek_shift, tgl_periksa) VALUES (%s,%s,%s,%s,%s,%s);
            '''
            if(response['peran'] == 'pengguna_publik' or response['peran'] == 'admin_satgas'):
                try:
                    with connection.cursor() as cursor:
                        cursor.execute(stmt1, [form.cleaned_data['nik'], noSTR, email, kodeFaskes, shift, tanggal])
                        connection.commit()
                    return redirect(url_dashboard)
                except Exception as e:
                    e = str(e)
                    print(e)
                    if ('duplicate key value violates unique constraint "memeriksa_pkey"' in e):
                        response['error'] = 'Pasien tidak bisa dibuatkan appointment karena telah terdaftar.'
                    elif('Jadwal dokter ini sudah penuh.') :
                        response['error'] = 'Jadwal dokter ini sudah penuh.'
                    else:
                        response['error'] = 'Terdapat sebuah kesalahan, silahkan coba lagi.'

    appointmentDokterForm = AppointmentDokterForm(request.POST)
    appointmentDokterForm.fields['nik'].widget.choices = get_list_nik(response['username'], response['peran'])
    appointmentDokterForm.fields['email'].widget.attrs['value'] = 'Email: ' + email
    appointmentDokterForm.fields['kodeFaskes'].widget.attrs['value'] = 'Kode Faskes: ' + kodeFaskes
    appointmentDokterForm.fields['shift'].widget.attrs['value'] = 'Shift: ' + shift
    appointmentDokterForm.fields['tanggal'].widget.attrs['value'] = 'Tanggal: ' + tanggal.strftime('%Y-%m-%d')
    response['form'] = appointmentDokterForm
    return render(request, 'app2/tambahAppointmentDenganDokter.html', response)

def get_list_nik(username, peran):
    if(peran == 'pengguna_publik'):
        stmt = '''
        SELECT nik FROM pasien WHERE idpendaftar='{0}';
        '''.format(username)
    elif (peran == 'admin_satgas'):
        stmt = '''
        SELECT nik FROM pasien;
        '''
    else:
        return []

    nik = tuple()
    try:
        with connection.cursor() as cursor:
            cursor.execute(stmt)
            nik = cursor.fetchall()
    except:
        pass
    return [('NIK Pasien', 'NIK Pasien')] + [(nikPasien[0], nikPasien[0]) for nikPasien in nik]

def listAppointmentDenganDokter(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)
    stmt1 = '''
    SELECT nik_pasien, nostr, username_dokter, kode_faskes, praktek_shift, tgl_periksa, rekomendasi FROM memeriksa, pasien WHERE nik = nik_pasien AND idpendaftar='{0}';
    '''.format(response['username'])
    stmt2 = '''
    SELECT nik_pasien, nostr, username_dokter, kode_faskes, praktek_shift, tgl_periksa, rekomendasi FROM memeriksa;
    '''
    stmt3 = '''
    SELECT nik_pasien, nostr, username_dokter, kode_faskes, praktek_shift, tgl_periksa, rekomendasi FROM memeriksa WHERE username_dokter='{0}';
    '''.format(response['username'])

    try:
        jadwal = tuple()
        if(response['peran'] == 'pengguna_publik'):
            with connection.cursor() as cursor:
                cursor.execute(stmt1)
                jadwal = cursor.fetchall()
            newJadwal = [(a[0], a[1], a[2], a[3], a[4], a[5].strftime('%Y-%m-%d'), a[6]) for a in jadwal]
            response['jadwal'] = newJadwal

        elif(response['peran'] == 'admin_satgas'):
            with connection.cursor() as cursor:
                cursor.execute(stmt2)
                jadwal = cursor.fetchall()
            newJadwal = [(a[0], a[1], a[2], a[3], a[4], a[5].strftime('%Y-%m-%d'), a[6]) for a in jadwal]
            response['jadwal'] = newJadwal

        elif(response['peran'] == 'dokter'):
            with connection.cursor() as cursor:
                cursor.execute(stmt3)
                jadwal = cursor.fetchall()
            newJadwal = [(a[0], a[1], a[2], a[3], a[4], a[5].strftime('%Y-%m-%d'), a[6]) for a in jadwal]
            response['jadwal'] = newJadwal
    except Exception as e:
        e = str(e)
        print(e)
    return render(request, 'app2/listAppointmentDenganDokter.html', response)

def updateAppointmentDenganDokter(request, nik, noSTR, email, kodeFaskes, shift, tanggal):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)
    tanggal = datetime.strptime(tanggal, '%Y-%m-%d').date()
    if (request.method == 'POST'):
        isian = request.POST.copy()
        isian['nik'] = nik
        isian['email'] = email
        isian['kodeFaskes'] = kodeFaskes
        isian['shift'] = shift
        isian['tanggal'] = tanggal
        form = UpdateAppointmentDokterForm(isian)
        if(form.is_valid()):
            stmt1 = '''
            UPDATE memeriksa SET rekomendasi=%s WHERE nik_pasien='{0}' AND nostr='{1}' AND username_dokter='{2}' AND kode_faskes='{3}' AND praktek_shift='{4}' AND tgl_periksa='{5}';
            '''.format(nik, noSTR, email, kodeFaskes, shift, tanggal)
            try:
                with connection.cursor() as cursor:
                    cursor.execute(stmt1, [form.cleaned_data['rekomendasi']])
                    connection.commit()
                return redirect(url_dashboard)
            except Exception as e:
                e = str(e)
                print(e)
    updateAppointmentDokterForm = UpdateAppointmentDokterForm(request.POST)
    updateAppointmentDokterForm.fields['email'].widget.attrs['value'] = 'Email Dokter: ' + email
    updateAppointmentDokterForm.fields['nik'].widget.attrs['value'] = 'NIK Pasien: ' + nik
    updateAppointmentDokterForm.fields['kodeFaskes'].widget.attrs['value'] = 'Faskes: ' + kodeFaskes
    updateAppointmentDokterForm.fields['shift'].widget.attrs['value'] = 'Shift Praktek: ' + shift
    updateAppointmentDokterForm.fields['tanggal'].widget.attrs['value'] = 'Tanggal Praktek: ' + tanggal.strftime('%Y-%m-%d')
    response['form'] = updateAppointmentDokterForm
    return render(request, 'app2/updateAppointmentDokter.html', response)

def deleteAppointmentDenganDokter(request, nik, noSTR, email, kodeFaskes, shift, tanggal):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)
    tanggal = datetime.strptime(tanggal, '%Y-%m-%d').date()
    if(response['peran'] == 'admin_satgas'):
        stmt1 = '''
        DELETE FROM MEMERIKSA WHERE nik_pasien='{0}' AND nostr='{1}' AND username_dokter='{2}' AND kode_faskes='{3}' AND praktek_shift='{4}' AND tgl_periksa='{5}';
        '''.format(nik, noSTR, email, kodeFaskes, shift, tanggal)
        try:
            with connection.cursor() as cursor:
                cursor.execute(stmt1)
                connection.commit()
            return redirect('app2:listAppointmentDenganDokter')
        except Exception as e:
            e = str(e)
            print(e)
    return redirect('app2:listAppointmentDenganDokter')

def buatRuanganRS(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)
    if (request.method == 'POST'):
        isian = request.POST.copy()
        isian['kode_ruangan'] = get_kode_ruangan(isian['koders'])
        form = CreateRuanganRSForm(isian)
        if(form.is_valid()):
            stmt1 = '''
            INSERT INTO ruangan_rs(koders, koderuangan, tipe, jmlbed, harga) VALUES (%s,%s,%s,%s,%s);
            '''
            if(response['peran'] == 'admin_satgas'):
                try:
                    with connection.cursor() as cursor:
                        cursor.execute(stmt1, [form.cleaned_data['koders'], form.cleaned_data['kode_ruangan'], form.cleaned_data['tipe'], 0, form.cleaned_data['harga']])
                        connection.commit()
                    return redirect('app2:listRuanganRS')
                except Exception as e:
                    e = str(e)
                    print(e)
                    response['error'] = 'Terdapat kesalahan dalam penulisan.'
    createRuanganRSForm = CreateRuanganRSForm(request.POST)
    response['form'] = createRuanganRSForm
    return render(request, 'app2/buatRuanganRS.html', response)

def get_kode_ruangan(koders):
    stmt = '''
    SELECT koderuangan FROM ruangan_rs WHERE koders = '{0}';
    '''.format(koders)
    with connection.cursor() as cursor:
        cursor.execute(stmt)
        kode_ruangan = cursor.fetchall()
    kode_baru = [(kode[0], kode[0]) for kode in kode_ruangan]
    count = 0
    if(len(kode_baru) > 0):
        for i in range(len(kode_ruangan)):
            count = max(count, int(kode_baru[i][0][1:len(kode_baru[i][0])]))
    return "R" + str(count + 1).zfill(4)

def dataKodeRuangan(request):
    if request.method == "POST":
        kode_rs = request.POST.get("koders")
        kode_ruangan = []
        kode_ruangan.append(get_kode_ruangan(kode_rs))
        data = {
            "data_kode_ruangan" : kode_ruangan
        }
    return JsonResponse(data)

def listRuanganRS(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)
    stmt1 = '''
    SELECT * FROM ruangan_rs;
    '''
    try:
        ruanganRS = tuple()
        if(response['peran'] == 'admin_satgas'):
            with connection.cursor() as cursor:
                cursor.execute(stmt1)
                ruanganRS = cursor.fetchall()
            response['ruanganRS'] = ruanganRS
    except Exception as e:
        e = str(e)
        print(e)
    return render(request, 'app2/listRuanganRS.html', response)

def updateRuanganRS(request, kodeRS, kodeRuangan):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)
    if (request.method == 'POST'):
        isian = request.POST.copy()
        isian['kodeRS'] = kodeRS
        isian['kodeRuangan'] = kodeRuangan
        form = UpdateRuanganRSForm(isian)
        if(form.is_valid()):
            stmt1 = '''
            UPDATE ruangan_rs SET tipe=%s, harga=%s  WHERE koderuangan='{0}' AND koders='{1}';
            '''.format(kodeRuangan,kodeRS)
            try:
                with connection.cursor() as cursor:
                    cursor.execute(stmt1, [form.cleaned_data['tipe'], form.cleaned_data['harga']])
                    connection.commit()
                return redirect('app2:listRuanganRS')
            except Exception as e:
                e = str(e)
                print(e)
                response['error'] = 'Terdapat kesalahan dalam penulisan.'
    updateRuanganRSForm = UpdateRuanganRSForm(request.POST)
    updateRuanganRSForm.fields['kodeRS'].widget.attrs['value'] = 'Kode RS: ' + kodeRS
    updateRuanganRSForm.fields['kodeRuangan'].widget.attrs['value'] = 'Kode Ruangan: ' + kodeRuangan
    response['form'] = updateRuanganRSForm
    return render(request, 'app2/updateRuanganRS.html', response)

def buatBedRS(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)
    if (request.method == 'POST'):
        isian = request.POST.copy()
        isian['kode_bed'] = get_kode_bed(isian['kode_ruangan'])
        form = CreateBedRSForm(isian)
        if(form.is_valid()):
            stmt1 = '''
            INSERT INTO bed_rs(koders, koderuangan, kodebed) VALUES (%s,%s,%s);
            '''
            if(response['peran'] == 'admin_satgas'):
                try:
                    with connection.cursor() as cursor:
                        cursor.execute(stmt1, [form.cleaned_data['kode_rs'], form.cleaned_data['kode_ruangan'], form.cleaned_data['kode_bed']])
                        connection.commit()
                    return redirect('app2:listBedRS')
                except Exception as e:
                    e = str(e)
                    print(e)

    createBedRSForm = CreateBedRSForm(request.POST)
    response['form'] = createBedRSForm
    return render(request, 'app2/buatBedRS.html', response)

def get_kode_bed(kode_ruangan):
    stmt = '''
    SELECT kodebed FROM bed_rs WHERE koderuangan = '{0}';
    '''.format(kode_ruangan)
    with connection.cursor() as cursor:
        cursor.execute(stmt)
        kode_bed = cursor.fetchall()
    kode_baru = [(kode[0], kode[0]) for kode in kode_bed]
    count = 0
    if(len(kode_baru) > 0):
        for i in range(len(kode_bed)):
            count = max(count, int(kode_baru[i][0][1:len(kode_baru[i][0])]))
    return "B" + str(count + 1).zfill(4)

def get_list_kode_ruangan(koders):
    stmt = '''
    SELECT koderuangan FROM ruangan_rs WHERE koders = '{0}';
    '''.format(koders)
    with connection.cursor() as cursor:
        cursor.execute(stmt)
        kode_ruangan = cursor.fetchall()
    
    return [('Kode Ruangan', 'Kode Ruangan')] + [(kode[0], kode[0]) for kode in kode_ruangan]

def jsonKodeRuangan(request):
    if request.method == "POST":
        kode_rs = request.POST.get("kode_rs")
        kode_ruangan_pilihan = request.POST.get("kode_ruangan")
        kode_ruangan = []
        kode_bed = []
        for kode in get_list_kode_ruangan(kode_rs):
            kode_ruangan.append(kode[0])
        kode_bed.append(get_kode_bed(kode_ruangan_pilihan))
        data = {
            "data_kode_ruangan" : kode_ruangan,
            "data_kode_bed" : kode_bed
        }
    return JsonResponse(data)

def listBedRS(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)
    stmt1 = '''
    SELECT * FROM bed_rs;
    '''
    try:
        bedRS = tuple()
        if(response['peran'] == 'admin_satgas'):
            with connection.cursor() as cursor:
                cursor.execute(stmt1)
                bedRS = cursor.fetchall()
            response['bedRS'] = bedRS
    except Exception as e:
        e = str(e)
        print(e)
    return render(request, 'app2/listBedRS.html', response)

def deleteBedRS(request, kodeRS, kodeRuangan, kodeBed):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)
    if(response['peran'] == 'admin_satgas'):
        stmt1 = '''
        DELETE FROM bed_rs WHERE koders='{0}' AND koderuangan='{1}' AND kodebed='{2}';
        '''.format(kodeRS, kodeRuangan, kodeBed)
        try:
            with connection.cursor() as cursor:
                cursor.execute(stmt1)
                connection.commit()
            return redirect('app2:listBedRS')
        except Exception as e:
            e = str(e)
            print(e)
    return redirect('app2:listBedRS')
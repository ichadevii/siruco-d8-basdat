from django.urls import re_path, path
from .views import *

app_name = 'app3'

urlpatterns = [
    path('reservasiRS/', reservasiRS, name='reservasiRS'),
    path('data/BasedOnKodeRS/', data_basedOnKodeRS, name='data_basedOnKodeRS'),
    path('listReservasiRS/', listReservasiRS, name='listReservasiRS'),
    path('updateReservasiRS/<str:kodepasien>/<str:tglMasuk>/', updateReservasiRS, name='updateReservasiRS'),
    path('submitUpdateReservasiRS/', submitUpdateReservasiRS, name='submitUpdateReservasiRS'),
    path('deleteReservasiRS/<str:kodepasien>/<str:tglMasuk>/', deleteReservasiRS, name='deleteReservasiRS'),

    path('buatJadwalFaskes/', buatJadwalFaskes, name='buatJadwalFaskes'),
    path('listJadwalFaskes/', listJadwalFaskes, name='listJadwalFaskes'),

    path('buatRS/', buatRS, name='buatRS'),
    path('listRumahSakit/', listRumahSakit, name='listRumahSakit'),
    path('updateRS/<str:kode_faskes>/', updateRS, name='updateRS'),
    path('submitUpdateRS/', submitUpdateRS, name='submitUpdateRS'),

    path('listTransaksiRS/', listTransaksiRS, name='listTransaksiRS'),
    path('updateTransaksiRS/<str:id_transaksi>/', updateTransaksiRS, name='updateTransaksiRS'),
    path('submitTransaksiRS/', submitTransaksiRS, name='submitTransaksiRS'),
    path('deleteTransaksiRS/<str:id_transaksi>/', deleteTransaksiRS, name='deleteTransaksiRS'),

    path('buatFaskes/', buatFaskes, name='buatFaskes'),
    path('listFaskes/', listFaskes, name='listFaskes'),
    path('updateFaskes/<str:kode>/', updateFaskes, name='updateFaskes'),
    path('submitFaskes/', submitFaskes, name='submitFaskes'),
    path('deleteFaskes/<str:kode>/', deleteFaskes, name='deleteFaskes'),
    path('detailFaskes/<str:kode>/', detailFaskes, name='detailFaskes'),
]
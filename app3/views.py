from django.shortcuts import render, redirect
from django.db import connection
from django.shortcuts import render
from .forms import *
from django.http import JsonResponse
from app1.views import *
import pandas as pd
import datetime as datetime

# RESERVASI RS
def reservasiRS(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)

    if (request.session.get('peran') not in ["admin_satgas"]):
        response["peran_diizinkan"] = ["Admin Satgas"]
        return render(request, "app3/error.html", response)

    if ((request.method == 'POST')):
        form = ReservasiRSForm(request.POST)
        
        if (form.is_valid()):
            stmt1 = '''
            INSERT INTO reservasi_rs(kodepasien, tglMasuk, tglKeluar, koders, koderuangan, kodebed) VALUES (%s,%s,%s,%s,%s,%s);
            '''
            kodePasien = request.POST.get('nik')
            tglMasuk = form.cleaned_data['tglMasuk']
            tglKeluar = form.cleaned_data['tglKeluar']
            koders = request.POST.get('kode_RS')
            koderuangan = form.cleaned_data['kode_ruangan']
            kodebed = form.cleaned_data['kode_bed']

            tglMasuk = pd.to_datetime(tglMasuk)
            tglKeluar = pd.to_datetime(tglKeluar)

            if (tglMasuk > tglKeluar):
                return redirect('/reservasiRS/')

            else :
                try:
                    with connection.cursor() as cursor:
                        cursor.execute(stmt1, [kodePasien, tglMasuk, tglKeluar, koders, koderuangan, kodebed])
                        connection.commit()
                    return redirect('/listReservasiRS/')
                except Exception as e:
                    e = str(e)
                    if ('duplicate key value violates unique constraint "reservasi_rs_pkey"' in e):
                        response['error'] = 'ID Pasien dan Tanggal Masuk telah terdaftar'
                    elif ('Tidak ada bed yang tersedia' in e):
                        response['error'] = 'Tidak ada bed yang tersedia pada RS dan Ruangan yang dipilih'
                    else :
                        response['error'] = 'Terjadi suatu kesalahan, silakan coba lagi nanti.'
    
    stmt1 = '''
    SELECT kode_faskes FROM RUMAH_SAKIT;
    '''
    kode_RS = tuple()
    try:
        with connection.cursor() as cursor:
            cursor.execute(stmt1)
            kode_RS = cursor.fetchall()
    except:
        pass

    stmt = '''
    SELECT nik FROM PASIEN;
    '''
    nik_pasien = tuple()
    try:
        with connection.cursor() as cursor:
            cursor.execute(stmt)
            nik_pasien = cursor.fetchall()
    except:
        pass

    response['listOfKodeRS'] = kode_RS
    response['listOfnik'] = nik_pasien
    form = ReservasiRSForm()
    response['form'] = form
    return render(request, 'app3/reservasiRS.html', response)

def data_basedOnKodeRS(request):
    if request.method == "POST":
        kode_RS = request.POST.get("kode_RS")

        list_items = []
        list_kode_ruangan = []
        list_kode_bed = []
        for kode in getKodeRuangan(kode_RS):
            list_kode_ruangan.append(kode[1]);
        
        for kode in getKodeBed(kode_RS):
            list_kode_bed.append(kode[1])
        
        item = {
            "list_kode_ruangan" : list_kode_ruangan,
            "list_kode_bed" : list_kode_bed
        }
        
    return JsonResponse(item)

def getKodeRuangan(kode_RS):
    stmt1 = '''
    SELECT koderuangan FROM RUANGAN_RS WHERE koders = %s ;
    '''
    with connection.cursor() as cursor:
        cursor.execute(stmt1, [kode_RS])
        kode_ruangan = cursor.fetchall()
    
    return [('', 'Kode Ruangan')] + [(kode[0], kode[0]) for kode in kode_ruangan]

def getKodeBed(kode_RS):
    stmt2 = '''
    SELECT kodebed FROM BED_RS WHERE koders = %s ;
    '''
    with connection.cursor() as cursor:
        cursor.execute(stmt2, [kode_RS])
        kode_bed = cursor.fetchall()

    return [('', 'Kode BED')] + [(kode[0], kode[0]) for kode in kode_bed]

def listReservasiRS(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)

    if (request.session.get('peran') not in ["admin_satgas", "pengguna_publik"]):
        response["peran_diizinkan"] = ["Admin Satgas", "Pengguna Publik"]
        return render(request, "app3/error.html", response)

    if (request.session.get('peran') in ["admin_satgas"]):
        stmt1 = '''
        SELECT * FROM RESERVASI_RS WHERE tglmasuk > %s;
        '''
        stmt2 = '''
        SELECT * FROM RESERVASI_RS WHERE tglmasuk <= %s;
        '''

        with connection.cursor() as cursor:
            cursor.execute(stmt1, [pd.to_datetime("today")])
            setOfReservasiRS = cursor.fetchall()
        
        with connection.cursor() as cursor:
            cursor.execute(stmt2, [pd.to_datetime("today")])
            ReservasiRSCantBeDeleted = cursor.fetchall()

    else:
        stmt1  = '''
        SELECT distinct R.kodepasien, R.tglmasuk, R.tglkeluar, R.koders, R.koderuangan, R.kodebed 
        FROM RESERVASI_RS R, PENGGUNA_PUBLIK PP, PASIEN P
        WHERE R.kodepasien = P.nik AND P.idpendaftar = %s;
        '''
        with connection.cursor() as cursor:
            cursor.execute(stmt1, [request.session.get('username')])
            setOfReservasiRS = cursor.fetchall()
        
        ReservasiRSCantBeDeleted = None    

    response['ReservasiRSCantBeDeleted'] = ReservasiRSCantBeDeleted
    response['setOfReservasiRS'] = setOfReservasiRS
    return render(request, 'app3/listReservasiRS.html', response)

def updateReservasiRS(request, kodepasien, tglMasuk):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)

    if (request.session.get('peran') not in ["admin_satgas"]):
        response["peran_diizinkan"] = ["Admin Satgas"]
        return render(request, "app3/error.html", response)

    stmt1 = '''
    SELECT * FROM RESERVASI_RS WHERE kodepasien = %s AND tglmasuk = %s;
    '''
    with connection.cursor() as cursor:
        cursor.execute(stmt1, [kodepasien, tglMasuk])
        reservasiRS = cursor.fetchall()

    form = ReservasiRSForm()

    form.fields["tglMasuk"].widget.attrs['disabled'] = True
    form.fields["tglMasuk"].initial = reservasiRS[0][1]
    form.fields["tglKeluar"].initial = reservasiRS[0][2]
    form.fields["kode_ruangan"].widget.attrs['disabled'] = True
    form.fields["kode_ruangan"].initial = reservasiRS[0][4]
    form.fields["kode_bed"].widget.attrs['disabled'] = True
    form.fields["kode_bed"].initial = reservasiRS[0][5]

    response['kodepasien'] = reservasiRS[0][0]
    response['koders'] = reservasiRS[0][3]

    response['dataReservasiRS'] = reservasiRS
    response['form'] = form
    return render (request, 'app3/updateReservasiRS.html', response)

def submitUpdateReservasiRS(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)

    if (request.session.get('peran') not in ["admin_satgas"]):
        response["peran_diizinkan"] = ["Admin Satgas"]
        return render(request, "app3/error.html", response)

    if(request.method == "POST"):
        form = ReservasiRSForm(request.POST)

        stmt1 = '''
        UPDATE RESERVASI_RS
        SET tglkeluar = %s WHERE kodepasien = %s AND tglmasuk = %s;
        '''
                
        kodepasien = request.POST.get('kodepasien')
        tglMasuk = request.POST.get('tglmasuk')
        tglKeluar = request.POST.get('tglkeluar')

        tglMasuk = pd.to_datetime(tglMasuk)
        tglKeluar = pd.to_datetime(tglKeluar)

        if (tglMasuk > tglKeluar):
            return redirect('/updateReservasiRS/'+ str(kodepasien) + '/' + str(tglMasuk) + '/')
            
        try:
            with connection.cursor() as cursor:
                cursor.execute(stmt1, [tglKeluar, kodepasien, tglMasuk])
                connection.commit()
                
        except:
            pass

    return redirect('/listReservasiRS/')

def deleteReservasiRS(request, kodepasien, tglMasuk):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)

    if (request.session.get('peran') not in ["admin_satgas"]):
        response["peran_diizinkan"] = ["Admin Satgas"]
        return render(request, "app3/error.html", response)

    if (request.method == 'POST'):
        stmt1 = '''
        DELETE FROM RESERVASI_RS WHERE kodepasien = %s AND tglmasuk = %s;
        '''
        try: 
            with connection.cursor() as cursor:
                cursor.execute(stmt1, [kodepasien, tglMasuk])
                connection.commit()
        except Exception as e:
            e = str(e)
            if ('violates foreign key constraint' in e):
                response['error'] = 'Reservasi RS tidak bisa dihapus karena ada atribut dari tabel lain yang menjadi foreign key dari Reservasi RS'
            else :
                response['error'] = 'Terjadi suatu kesalahan, silakan coba lagi nanti.'

    return redirect('/listReservasiRS/')
# RESERVASI RS

# JADWAL FASKES
def buatJadwalFaskes(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)

    if (request.session.get('peran') not in ["admin_satgas"]):
        response["peran_diizinkan"] = ["Admin Satgas"]
        return render(request, "app3/error.html", response)

    if ((request.method == 'POST')):
        form = JadwalFaskesForm(request.POST)
        
        if (form.is_valid()):
            # Ventilator harusnya ilang
            stmt1 = '''
            INSERT INTO jadwal(kode_faskes, shift, tanggal) VALUES (%s,%s,%s);
            '''
            kode_faskes = request.POST.get('faskes')
            shift = form.cleaned_data['shift']
            tanggal = form.cleaned_data['tanggal']
        
            try:
                with connection.cursor() as cursor:
                    cursor.execute(stmt1, [kode_faskes, shift, tanggal])
                    connection.commit()
                return redirect('/listJadwalFaskes/')
            except Exception as e:
                e = str(e)
                if ('already exists' in e):
                    response['error'] = 'Email telah digunakan, silakan gunakan email lain'
                elif ('Password harus memiliki minimal satu huruf kapital dan satu angka.' in e):
                    response['error'] = 'Password Anda belum memenuhi syarat, silahkan pastikan bahwa password minimal terdapat 1 huruf kapital dan 1 angka'
                
    stmt = '''
    SELECT kode FROM FASKES;
    '''
    kode_faskes = tuple()
    with connection.cursor() as cursor:
        cursor.execute(stmt)
        kode_faskes = cursor.fetchall()

    response['listOfKodeFaskes'] = kode_faskes
    form = JadwalFaskesForm()
    response['form'] = form
    return render (request, 'app3/buatJadwalFaskes.html', response)

def listJadwalFaskes(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)

    if (request.session.get('peran') not in ["admin_satgas"]):
        response["peran_diizinkan"] = ["Admin Satgas"]
        return render(request, "app3/error.html", response)

    stmt1 = '''
    SELECT * FROM JADWAL
    '''
    with connection.cursor() as cursor:
        cursor.execute(stmt1)
        setOfJadwalFaskes = cursor.fetchall()

    response['setOfJadwalFaskes'] = setOfJadwalFaskes
    return render(request, 'app3/listJadwalFaskes.html', response)
# JADWAL FASKES

# RUMAH SAKIT
def buatRS(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)

    if (request.session.get('peran') not in ["admin_satgas"]):
        response["peran_diizinkan"] = ["Admin Satgas"]
        return render(request, "app3/error.html", response)

    if ((request.method == 'POST')):
        form = RumahSakitForm(request.POST)
        
        if (form.is_valid()):
            # Ventilator harusnya ilang
            stmt1 = '''
            INSERT INTO RUMAH_SAKIT(kode_faskes, isrujukan) VALUES (%s,%s);
            '''
            faskes = request.POST.get('faskes')
            rujukan = int(form.cleaned_data['rujukan'])
            rujukan = str(rujukan)

            try:
                with connection.cursor() as cursor:
                    cursor.execute(stmt1, [faskes, rujukan])
                    connection.commit()
                return redirect('/listRumahSakit/')
            except Exception as e:
                e = str(e)
                if ('duplicate key value violates unique constraint "rumah_sakit_pkey"' in e):
                        response['error'] = 'Kode Faskes yang digunakan telah terdaftar'
                else :
                    response['error'] = 'Terjadi suatu kesalahan, silakan coba lagi nanti.'
        

    stmt = '''
    SELECT kode FROM FASKES;
    '''
    kode_faskes = tuple()
    with connection.cursor() as cursor:
        cursor.execute(stmt)
        kode_faskes = cursor.fetchall()

    response['listOfKodeFaskes'] = kode_faskes
    form = RumahSakitForm()
    response['form'] = form
    return render(request, 'app3/buatRS.html', response)

def listRumahSakit(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)

    if (request.session.get('peran') not in ["admin_satgas"]):
        response["peran_diizinkan"] = ["Admin Satgas"]
        return render(request, "app3/error.html", response)

    stmt1 = '''
    SELECT * FROM RUMAH_SAKIT;
    '''
    with connection.cursor() as cursor:
        cursor.execute(stmt1)
        setOfRS = cursor.fetchall()

    response['setOfRS'] = setOfRS
    return render(request, 'app3/listRumahSakit.html', response)

def updateRS(request, kode_faskes):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)

    if (request.session.get('peran') not in ["admin_satgas"]):
        response["peran_diizinkan"] = ["Admin Satgas"]
        return render(request, "app3/error.html", response)

    stmt1 = '''
    SELECT * FROM RUMAH_SAKIT WHERE kode_faskes = %s ;
    '''
    with connection.cursor() as cursor:
        cursor.execute(stmt1, [kode_faskes])
        rumahSakit = cursor.fetchall()

    form = RumahSakitForm()

    form.fields['rujukan'].initial = (bool)((int)(rumahSakit[0][1]))

    response['kodefaskes'] = rumahSakit[0][0]
    response['dataRS'] = rumahSakit
    response['form'] = form
    return render (request, 'app3/updateRumahSakit.html', response)

def submitUpdateRS(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)

    if (request.session.get('peran') not in ["admin_satgas"]):
        response["peran_diizinkan"] = ["Admin Satgas"]
        return render(request, "app3/error.html", response)

    if (request.method == "POST"):
        form = RumahSakitForm(request.POST)
        
        stmt1 = '''
        UPDATE RUMAH_SAKIT
        SET isrujukan = %s 
        WHERE kode_faskes = %s;
        '''
        
        rujukan = str(request.POST.get('isrujukan'))
        kode_faskes = request.POST.get('kode_faskes')
        try:
            with connection.cursor() as cursor:
                cursor.execute(stmt1, [rujukan, kode_faskes])
                connection.commit()
                
        except Exception as e:
            pass
    return redirect('/listRumahSakit/')
# RUMAH SAKIT

# TRANSAKSI RS 
def listTransaksiRS(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)

    if (request.session.get('peran') not in ["admin_satgas"]):
        response["peran_diizinkan"] = ["Admin Satgas"]
        return render(request, "app3/error.html", response)

    stmt1 = '''
    SELECT * FROM TRANSAKSI_RS;
    '''
    with connection.cursor() as cursor:
        cursor.execute(stmt1)
        setOfTransaksiRS = cursor.fetchall()

    response['setOfTransaksiRS'] = setOfTransaksiRS
    return render(request, 'app3/listTransaksiRS.html', response)

def updateTransaksiRS(request, id_transaksi):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)

    if (request.session.get('peran') not in ["admin_satgas"]):
        response["peran_diizinkan"] = ["Admin Satgas"]
        return render(request, "app3/error.html", response)

    stmt1 = '''
    SELECT * FROM TRANSAKSI_RS WHERE idtransaksi = %s ;
    '''
    with connection.cursor() as cursor:
        cursor.execute(stmt1, [id_transaksi])
        transaksiRS = cursor.fetchall()

    form = TransaksiRSForm()

    i = 0
    for field in form.fields:
        form.fields[field].initial = transaksiRS[0][i]
        if(field != "statusBayar"):
            form.fields[field].widget.attrs['disabled'] = True
            form.fields[field].required = False
        else : 
            form.fields[field].widget.attrs['disabled'] = False
        i += 1;

    response['dataTransaksiRS'] = transaksiRS
    response['form'] = form
    return render (request, 'app3/updateTransaksiRS.html', response)

def submitTransaksiRS(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)

    if (request.session.get('peran') not in ["admin_satgas"]):
        response["peran_diizinkan"] = ["Admin Satgas"]
        return render(request, "app3/error.html", response)

    if (request.method == "POST"):
        form = TransaksiRSForm(request.POST)
        
        stmt1 = '''
        UPDATE TRANSAKSI_RS
        SET statusbayar = %s 
        WHERE idtransaksi = %s;
        '''

        idtransaksi = request.POST.get('idtransaksi')
        statusbayar = request.POST.get('statusbayar')
        try:
            with connection.cursor() as cursor:
                cursor.execute(stmt1, [statusbayar, idtransaksi])
                connection.commit()
                
        except Exception as e:
            pass
    return redirect('/listTransaksiRS/')

def deleteTransaksiRS(request, id_transaksi):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)

    if (request.session.get('peran') not in ["admin_satgas"]):
        response["peran_diizinkan"] = ["Admin Satgas"]
        return render(request, "app3/error.html", response)

    if (request.method == 'POST'):
        stmt1 = '''
        DELETE FROM TRANSAKSI_RS WHERE idtransaksi = %s;
        '''
        try: 
            with connection.cursor() as cursor:
                cursor.execute(stmt1, [id_transaksi])
                connection.commit()
        except Exception as e:
            e = str(e)
            if ('violates foreign key constraint' in e):
                response['error'] = 'Transaksi RS tidak bisa dihapus karena ada atribut dari tabel lain yang menjadi foreign key dari Transaksi RS'
            else :
                response['error'] = 'Terjadi suatu kesalahan, silakan coba lagi nanti.'
            

    return redirect('/listTransaksiRS/')

# FASKES
def buatFaskes(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)

    if (request.session.get('peran') not in ["admin_satgas"]):
        response["peran_diizinkan"] = ["Admin Satgas"]
        return render(request, "app3/error.html", response)

    if ((request.method == 'POST')):
        form = FasilitasKesehatanForm(request.POST)
        
        if (form.is_valid()):
            stmt1 = '''
            INSERT INTO faskes(kode, tipe, nama, statusmilik, jalan, kelurahan, kecamatan, kabkot, prov) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s);
            '''
            kode = request.POST.get("kodeFaskes-hidden")
            tipe = form.cleaned_data['tipeFaskes']
            nama = form.cleaned_data['namaFaskes']
            statusmilik = form.cleaned_data['statusKepemilikan']
            jalan = form.cleaned_data['jalanFaskes']
            kelurahan = form.cleaned_data['kelurahanFaskes']
            kecamatan = form.cleaned_data['kecamatanFaskes']
            kabkot = form.cleaned_data['kabkotFaskes']
            prov = form.cleaned_data['provinsiFaskes']
        
            try:
                with connection.cursor() as cursor:
                    cursor.execute(stmt1, [kode, tipe, nama, statusmilik, jalan, kelurahan, kecamatan, kabkot, prov])
                    connection.commit()
                return redirect('/listFaskes/')
            except Exception as e:
                e = str(e)
                if ('duplicate key value violates unique constraint "faskes_pkey"' in e):
                    response['error'] = 'Kode Faskes yang digunakan telah terdaftar'
                else :
                    response['error'] = 'Terjadi suatu kesalahan, silakan coba lagi nanti.'

    stmt1 = '''
    SELECT MAX(kode) FROM FASKES;
    '''
    with connection.cursor() as cursor:
        cursor.execute(stmt1)
        lastKodeFaskes = cursor.fetchall()
    lastKodeFaskes = lastKodeFaskes[0][0]

    if (lastKodeFaskes != None):
        lastKodeFaskesNum = lastKodeFaskes[2:len(lastKodeFaskes)]
        lastKodeFaskesNum = int(lastKodeFaskesNum)
        newKodeFaskesNum = lastKodeFaskesNum + 1
        newKodeFaskes = 'F' + str(newKodeFaskesNum).zfill(2)
    else :
        newKodeFaskes = "F01"

    response["kodeFaskes"] = newKodeFaskes
    form = FasilitasKesehatanForm()
    response['form'] = form
    return render (request, 'app3/buatFaskes.html', response)

def listFaskes(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)

    if (request.session.get('peran') not in ["admin_satgas"]):
        response["peran_diizinkan"] = ["Admin Satgas"]
        return render(request, "app3/error.html", response)

    stmt1 = '''
    SELECT kode, tipe, nama FROM FASKES;
    '''
    with connection.cursor() as cursor:
        cursor.execute(stmt1)
        setOfFaskes = cursor.fetchall()

    response['setOfFaskes'] = setOfFaskes
    return render(request, 'app3/listFaskes.html', response)

def updateFaskes(request, kode):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)

    if (request.session.get('peran') not in ["admin_satgas"]):
        response["peran_diizinkan"] = ["Admin Satgas"]
        return render(request, "app3/error.html", response)

    stmt1 = '''
    SELECT * FROM FASKES WHERE kode = %s ;
    '''
    with connection.cursor() as cursor:
        cursor.execute(stmt1, [kode])
        faskes = cursor.fetchall()

    form = FasilitasKesehatanForm()

    i = 1
    for field in form.fields:
        form.fields[field].initial = faskes[0][i]
        i += 1

    response['kodeFaskes'] = faskes[0][0]
    response['form'] = form
    return render (request, 'app3/updateFasilitasKesehatan.html', response)

def submitFaskes(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)

    if (request.session.get('peran') not in ["admin_satgas"]):
        response["peran_diizinkan"] = ["Admin Satgas"]
        return render(request, "app3/error.html", response)

    if (request.method == "POST"):

        form = FasilitasKesehatanForm(request.POST)
        if (form.is_valid()):
            stmt1 = '''
            UPDATE FASKES
            SET tipe = %s,
                nama = %s,
                statusmilik = %s,
                jalan = %s,
                kelurahan = %s,
                kecamatan = %s,
                kabkot = %s,
                prov = %s
            WHERE kode = %s;
            '''
            kode = request.POST.get("kodeFaskes-hidden")
            tipe = form.cleaned_data['tipeFaskes']
            nama = form.cleaned_data['namaFaskes']
            statusmilik = form.cleaned_data['statusKepemilikan']
            jalan = form.cleaned_data['jalanFaskes']
            kelurahan = form.cleaned_data['kelurahanFaskes']
            kecamatan = form.cleaned_data['kecamatanFaskes']
            kabkot = form.cleaned_data['kabkotFaskes']
            prov = form.cleaned_data['provinsiFaskes']

            try:
                with connection.cursor() as cursor:
                    cursor.execute(stmt1, [tipe, nama, statusmilik, jalan, kelurahan, kecamatan, kabkot, prov, kode])
                    connection.commit()
                    
            except Exception as e:
                pass
    return redirect('/listFaskes/')

def deleteFaskes(request, kode):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)

    if (request.session.get('peran') not in ["admin_satgas"]):
        response["peran_diizinkan"] = ["Admin Satgas"]
        return render(request, "app3/error.html", response)

    if (request.method == "POST"):
        stmt1 = '''
        DELETE FROM FASKES WHERE kode = %s;
        '''
        try: 
            with connection.cursor() as cursor:
                cursor.execute(stmt1, [kode])
                connection.commit()
        except Exception as e:
            e = str(e)
            if ('violates foreign key constraint' in e):
                response['error'] = 'Faskes tidak bisa dihapus karena ada atribut dari tabel lain yang menjadi foreign key dari faskes'
            else :
                response['error'] = 'Terjadi suatu kesalahan, silakan coba lagi nanti.'

    return redirect('/listFaskes/')

def detailFaskes(request, kode):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)

    if (request.session.get('peran') not in ["admin_satgas"]):
        response["peran_diizinkan"] = ["Admin Satgas"]
        return render(request, "app3/error.html", response)

    stmt1 = '''
    SELECT * FROM FASKES WHERE kode = %s;
    '''
    with connection.cursor() as cursor:
        cursor.execute(stmt1, [kode])
        setOfFaskes = cursor.fetchall()

    response['setOfFaskes'] = setOfFaskes
    return render(request, 'app3/detailFaskes.html', response)


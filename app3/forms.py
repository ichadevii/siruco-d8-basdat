from django import forms
from django.db import connection

class ReservasiRSForm(forms.Form):

    tglMasuk_attrs = {
        'type' : 'date',
        'class' : 'form-control RS',
        'placeholder' : 'Tanggal Masuk',
    }

    tglKeluar_attrs = {
        'type' : 'date',
        'class' : 'form-control RS',
        'placeholder' : 'Tanggal Keluar',
        'id' : 'tglkeluar'
    }

    kode_ruangan_attrs = {
        'class' : 'form-control RS',
        'maxlength': 5,
        'placeholder' : 'Kode Ruangan',
        'id' : 'select-kode-ruangan'
    }

    kode_bed_attrs = {
        'class' : 'form-control RS',
        'maxlength': 5,
        'placeholder' : 'Kode Bed',
        'id' : 'select-kode-bed'
    }
    
    def getKodeRuangan():
        stmt1 = '''
        SELECT koderuangan FROM RUANGAN_RS;
        '''
        with connection.cursor() as cursor:
            cursor.execute(stmt1)
            kode_ruangan = cursor.fetchall()
        
        return [('', 'Kode Ruangan')] + [(kode[0], kode[0]) for kode in kode_ruangan]

    def getKodeBed():
        stmt2 = '''
        SELECT kodebed FROM BED_RS;
        '''
        with connection.cursor() as cursor:
            cursor.execute(stmt2)
            kode_bed = cursor.fetchall()

        return [('', 'Kode BED')] + [(kode[0], kode[0]) for kode in kode_bed]

    KODE_RUANGAN_CHOICES = getKodeRuangan()
    KODE_BED_CHOICES = getKodeBed()

    tglMasuk = forms.DateField(widget=forms.DateInput(attrs=tglMasuk_attrs))
    tglKeluar = forms.DateField(widget=forms.DateInput(attrs=tglKeluar_attrs))
    kode_ruangan = forms.ChoiceField(choices=KODE_RUANGAN_CHOICES, widget=forms.Select(attrs=kode_ruangan_attrs))
    kode_bed = forms.ChoiceField(choices=KODE_BED_CHOICES, widget=forms.Select(attrs=kode_bed_attrs))

class JadwalFaskesForm(forms.Form):

    shift_attrs = {
        'class' : 'form-control Faskes',
        'required' : True,
        'placeholder' : 'Shift',
        'maxlength': 15,
    }

    tanggal_attrs = {
        'type' : 'date',
        'class' : 'form-control Faskes',
        'required' : True,
        'placeholder' : 'Tanggal',
    }

    SHIFT_CHOICES = [
        ('', 'Shift'),
        ('Pagi', 'Pagi'),
        ('Siang', 'Siang'),
        ('Sore', 'Sore'),
        ('Malam', 'Malam')]

    shift = forms.ChoiceField(choices=SHIFT_CHOICES, widget=forms.Select(attrs=shift_attrs))
    tanggal = forms.DateField(widget=forms.DateInput(attrs=tanggal_attrs))
    

class RumahSakitForm(forms.Form):

    rujukan_attrs = {
        'type' : 'checkbox',
        'required' : False,
        'label' : 'Rujukan',
        'id' : 'isrujukan',
        'maxlength': 1,
    }

    rujukan = forms.BooleanField(widget=forms.CheckboxInput(attrs=rujukan_attrs), required=False, initial=False)
 
class TransaksiRSForm(forms.Form):
    id_transaksi_attrs = {
        'type' : 'text',
        'placeholder' : 'ID Transaksi',
        'class' : 'form-control RS',
        'maxlength': 10,
    }

    kodepasien_attrs = {
        'type' : 'text',
        'placeholder' : 'Kode Pasien',
        'class' : 'form-control RS',
        'maxlength': 20,
    }

    tanggalPembayaran_attrs = {
        'type' : 'date',
        'class' : 'form-control RS',
        'placeholder' : 'Tanggal Pembayaran',
    }

    waktuPembayaran_attrs = {
        'type' : 'timezone',
        'class' : 'form-control RS',
        'placeholder' : 'Waktu Pembayaran',
    }

    tanggalMasuk_attrs = {
        'type' : 'date',
        'class' : 'form-control RS',
        'placeholder' : 'Tanggal Masuk',
    }

    totalBiaya_attrs = {
        'type' : 'text',
        'class' : 'form-control RS',
        'placeholder' : 'Total Biaya',
    }

    statusBayar_attrs = {
        'type' : 'text',
        'class' : 'form-control RS',
        'placeholder' : 'Status Bayar',
        'maxlength': 15,
        'id' : 'statusBayar'
    }

    STATUS_BAYAR_CHOICES = [
        ('', 'Status Bayar'),
        ('Lunas', 'Lunas'),
        ('Belum Lunas', 'Belum Lunas'),
        ('Bayar Invalid', 'Bayar Invalid'),
        ('Bayar Duplikat', 'Bayar Duplikat')]

    id_transaksi = forms.CharField(widget=forms.TextInput(attrs=id_transaksi_attrs))
    kodepasien = forms.SlugField(widget=forms.TextInput(attrs=kodepasien_attrs))
    tanggalPembayaran = forms.DateField(widget=forms.DateInput(attrs=tanggalPembayaran_attrs))
    waktuPembayaran = forms.TimeField(widget=forms.TimeInput(attrs=waktuPembayaran_attrs))
    tanggalMasuk = forms.DateField(widget=forms.DateInput(attrs=tanggalMasuk_attrs))
    totalBiaya = forms.IntegerField(widget=forms.TextInput(attrs=totalBiaya_attrs))
    statusBayar = forms.ChoiceField(choices=STATUS_BAYAR_CHOICES, widget=forms.Select(attrs=statusBayar_attrs))

class FasilitasKesehatanForm(forms.Form):
    kode_faskes_attrs = {
        'type' : 'text',
        'placeholder' : 'Kode Faskes',
        'class' : 'form-control Faskes',
        'maxlength': 3,
    }

    tipe_faskes_attrs = {
        'placeholder' : 'Tipe',
        'class' : 'form-control Faskes',
        'maxlength': 30,
    }

    nama_faskes_attrs = {
        'type' : 'text',
        'placeholder' : 'Nama',
        'class' : 'form-control Faskes',
        'maxlength': 50,
    }

    status_kepemilikan_attrs = {
        'placeholder' : 'Status Kepemilikan',
        'class' : 'form-control Faskes',
        'maxlength': 30,
    }

    jalan_attrs = {
        'type' : 'text',
        'placeholder' : 'Jalan',
        'class' : 'form-control Faskes',
        'maxlength': 30,
    }

    kelurahan_attrs = {
        'type' : 'text',
        'placeholder' : 'Kelurahan',
        'class' : 'form-control Faskes',
        'maxlength': 30,
    }

    kecamatan_attrs = {
        'type' : 'text',
        'placeholder' : 'Kecamatan',
        'class' : 'form-control Faskes',
        'maxlength': 30,
    }

    kabkot_attrs = {
        'type' : 'text',
        'placeholder' : 'Kebupaken/Kota',
        'class' : 'form-control Faskes',
        'maxlength': 30,
    }

    provinsi_attrs = {
        'type' : 'text',
        'placeholder' : 'Provinsi',
        'class' : 'form-control Faskes',
        'maxlength': 30,
    }

    TIPE_FASKES_CHOICES = [
        ('', 'Tipe'),
        ('Puskesmas', 'Puskesmas'),
        ('Klinik', 'Klinik'),
        ('Rumah Sakit', 'Rumah Sakit')]
    
    STATUS_KEPEMILIKAN_CHOICES = [
        ('', 'Status Kepemilikan'),
        ('Hak Milik', 'Hak Milik'),
        ('Pemerintah', 'Pemerintah'),
        ('Perusahaan', 'Perusahaan')]

    tipeFaskes = forms.ChoiceField(choices=TIPE_FASKES_CHOICES, widget=forms.Select(attrs=tipe_faskes_attrs), required=True)
    namaFaskes = forms.CharField(widget=forms.TextInput(attrs=nama_faskes_attrs), required=True)
    statusKepemilikan = forms.ChoiceField(choices=STATUS_KEPEMILIKAN_CHOICES, widget=forms.Select(attrs=status_kepemilikan_attrs), required=True)
    jalanFaskes = forms.CharField(widget=forms.TextInput(attrs=jalan_attrs), required=True)
    kelurahanFaskes = forms.CharField(widget=forms.TextInput(attrs=kelurahan_attrs), required=True)
    kecamatanFaskes = forms.CharField(widget=forms.TextInput(attrs=kecamatan_attrs), required=True)
    kabkotFaskes = forms.CharField(widget=forms.TextInput(attrs=kabkot_attrs), required=True)
    provinsiFaskes = forms.CharField(widget=forms.TextInput(attrs=provinsi_attrs), required=True)
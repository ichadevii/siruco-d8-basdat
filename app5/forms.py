from django import forms

class PaketMakanForm(forms.Form):
    kode_hotel_attrs = {
        'placeholder' : 'Kode Hotel',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 5,
    }

    kode_paket_attrs = {
        'placeholder' : 'Kode Paket',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 5,
    }
    
    nama_attrs = {
        'placeholder' : 'Nama',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 20,
    }
    
    harga_attrs = {
        'type': 'number',
        'min': 0,
        'placeholder' : 'Harga',
        'class' : 'form-control',
        'required' : True,
    }

    kode_hotel = forms.CharField(widget=forms.TextInput(attrs=kode_hotel_attrs))
    kode_paket = forms.CharField(widget=forms.TextInput(attrs=kode_paket_attrs))
    nama = forms.CharField(widget=forms.TextInput(attrs=nama_attrs))
    harga = forms.IntegerField(widget=forms.TextInput(attrs=harga_attrs))

class HotelForm(forms.Form):
    nama_attrs = {
        'placeholder' : 'Nama',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 30,
    }

    is_rujukan_attrs = {
        'class' : 'form-control',
        'required' : False,
    }

    jalan_attrs = {
        'placeholder' : 'Jalan',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 30,
    }

    kelurahan_attrs = {
        'placeholder' : 'Kelurahan',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 30,
    }

    kecamatan_attrs = {
        'placeholder' : 'Kecamatan',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 30,
    }

    kabkot_attrs = {
        'placeholder' : 'Kabupaten / Kota',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 30,
    }

    prov_attrs = {
        'placeholder' : 'Provinsi',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 30,
    }

    nama = forms.CharField(widget=forms.TextInput(attrs=nama_attrs))
    is_rujukan = forms.BooleanField(widget=forms.CheckboxInput(attrs=is_rujukan_attrs), required=False)
    jalan = forms.CharField(widget=forms.TextInput(attrs=jalan_attrs))
    kelurahan = forms.CharField(widget=forms.TextInput(attrs=kelurahan_attrs))
    kecamatan = forms.CharField(widget=forms.TextInput(attrs=kecamatan_attrs))
    kabkot = forms.CharField(widget=forms.TextInput(attrs=kabkot_attrs))
    prov = forms.CharField(widget=forms.TextInput(attrs=prov_attrs))
    
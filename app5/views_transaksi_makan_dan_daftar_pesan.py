from django.shortcuts import render, redirect
from django.db import connection
from .forms import *
from django.http import JsonResponse, Http404

url_dashboard = 'app1:dashboard'
url_login = 'app1:login'
url_list_transaksi_makan = 'app5:listTransaksiMakan'
ERROR_TEMPLATE = 'app5/error.html'
ADMIN_SATGAS = 'admin_satgas'
PENGGUNA_PUBLIK = 'pengguna_publik'

def set_username(request, response):
    response['is_authenticated'] = request.session.get('is_authenticated')
    response['username'] = request.session.get('username')
    response['peran'] = request.session.get('peran')

def create_transaksi_makan(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)

    response = {}
    set_username(request, response)

    # Create dilakukan admin_satgas atau pengguna_publik
    if (request.session.get('peran') not in [ADMIN_SATGAS, PENGGUNA_PUBLIK]):
        return render(request, ERROR_TEMPLATE, response)

    if (request.method == 'POST'):
        try:
            id_transaksi = request.POST['idTransaksi']
            kode_paket = request.POST.getlist('kodePaket')
            id_transaksi_makan = request.POST['idTransaksiMakan']
            kode_hotel = request.POST['kodeHotel']

            # memasukkan ke tabel TRANSAKSI_MAKAN
            stmt = '''
            INSERT INTO TRANSAKSI_MAKAN(idtransaksi, idtransaksimakan)
            VALUES (%s, %s)
            '''
            with connection.cursor() as cursor:
                cursor.execute(stmt, [id_transaksi, id_transaksi_makan])
                connection.commit()

            # memasukkan semua kode paket ke daftar pesan
            stmt = '''
            INSERT INTO DAFTAR_PESAN(idtransaksimakan, id_transaksi, kodehotel, kodepaket)
            VALUES (%s, %s, %s, %s)
            '''
            for i in range(len(kode_paket)):
                with connection.cursor() as cursor:
                    cursor.execute(stmt, [id_transaksi_makan, id_transaksi, kode_hotel, kode_paket[i]])
                    connection.commit()
            
            return redirect(url_list_transaksi_makan)
        except Exception as e:
            print(e)
            response['error'] = 'Terjadi suatu kesalahan, silakan coba lagi nanti.'

    if (request.session.get('peran') == ADMIN_SATGAS):
        # admin_satgas dapat membuat transaksi makan untuk transaksi hotel siapapun
        stmt = '''
        SELECT idtransaksi FROM TRANSAKSI_HOTEL;
        '''
        try:
            with connection.cursor() as cursor:
                cursor.execute(stmt)
                response['idTransaksi'] = [x[0] for x in cursor.fetchall()]
        except:
            pass
    else:
        # pengguna publik dapat membuat transaksi makan untuk transaksi hotel
        # yang dibuat untuk pasien yang didaftarkannya
        stmt = '''
        SELECT th.idtransaksi FROM TRANSAKSI_HOTEL th
        JOIN PASIEN p
        ON p.nik = th.kodepasien
        WHERE p.idpendaftar = %s;
        '''
        try:
            with connection.cursor() as cursor:
                cursor.execute(stmt, [request.session.get('username')])
                response['idTransaksi'] = [x[0] for x in cursor.fetchall()]
        except:
            pass

    # mendapatkan id_transaksi_makan
    stmt = '''
    SELECT idtransaksimakan
    FROM TRANSAKSI_MAKAN
    ORDER BY idtransaksimakan DESC
    LIMIT 1;
    '''
    try:
        with connection.cursor() as cursor:
            cursor.execute(stmt)
            nomor_transaksi_makan = cursor.fetchall()

        if (len(nomor_transaksi_makan) == 0):
            nomor_transaksi_makan = 0
        else:
            nomor_transaksi_makan = int(nomor_transaksi_makan[0][0][3:])
        response['idTransaksiMakan'] = 'TRM' + str(nomor_transaksi_makan + 1).zfill(7)
    except:
        pass

    return render(request, 'app5/create_transaksi_makan.html', response)

def get_kode_hotel_from_id_transaksi(request):
    response = {}
    stmt = '''
    SELECT rh.kodehotel
    FROM RESERVASI_HOTEL rh, TRANSAKSI_BOOKING tb
    WHERE tb.idtransaksibooking = %s
        AND tb.kodepasien = rh.kodepasien
        AND tb.tglmasuk = rh.tglmasuk;
    '''
    try:
        with connection.cursor() as cursor:
            cursor.execute(stmt, [request.GET['idTransaksi']])
            response['kodeHotel'] = cursor.fetchone()[0]
    except:
        pass

    return JsonResponse(response)

def get_kode_paket_from_id_transaksi(request):
    response = {}
    stmt1 = '''
    SELECT rh.kodehotel
    FROM RESERVASI_HOTEL rh, TRANSAKSI_BOOKING tb
    WHERE tb.idtransaksibooking = %s
        AND tb.kodepasien = rh.kodepasien
        AND tb.tglmasuk = rh.tglmasuk;
    '''
    stmt2 = '''
    SELECT kodepaket
    FROM paket_makan
    WHERE kodehotel = %s;
    '''

    try:
        with connection.cursor() as cursor:
            cursor.execute(stmt1, [request.GET['idTransaksi']])
            kode_hotel = cursor.fetchone()[0]
        with connection.cursor() as cursor:
            cursor.execute(stmt2, [kode_hotel])
            res = cursor.fetchall()
            response['kodePaket'] = [x[0] for x in res]
    except:
        pass

    return JsonResponse(response)

def list_transaksi_makan(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)

    response = {}
    set_username(request, response)

    # hanya admin satgas dan pengguna publik yang dapat melakukan operasi ini
    if (request.session.get('peran') not in [ADMIN_SATGAS, PENGGUNA_PUBLIK]):
        return render(request, ERROR_TEMPLATE, response)

    # langsung dapatkan semua field yang digunakan pada
    # list transaksi makan
    if (request.session.get('peran') == PENGGUNA_PUBLIK):
        # jika yang membaca pengguna publik, maka 
        # dia hanya bisa membaca transaksi yang dibuatnya
        stmt = '''
        SELECT tm.idtransaksi, tm.idtransaksimakan, tm.totalbayar, th.statusbayar
        FROM TRANSAKSI_MAKAN tm
        JOIN TRANSAKSI_HOTEL th
        ON th.idtransaksi = tm.idtransaksi
        JOIN PASIEN p
        ON p.nik = th.kodepasien
        WHERE p.idpendaftar = %s;
        '''
        try:
            with connection.cursor() as cursor:
                cursor.execute(stmt, [request.session.get('username')])
                response['transaksiMakan'] = cursor.fetchall()
        except:
            pass
    else:
        # jika yang membaca admin satgas, maka
        # dia bisa membaca seluruh transaksi makan
        stmt = '''
        SELECT tm.idtransaksi, tm.idtransaksimakan, tm.totalbayar, th.statusbayar
        FROM TRANSAKSI_MAKAN tm
        JOIN TRANSAKSI_HOTEL th
        ON th.idtransaksi = tm.idtransaksi
        '''
        try:
            with connection.cursor() as cursor:
                cursor.execute(stmt)
                response['transaksiMakan'] = cursor.fetchall()
        except:
            pass
            
    return render(request, 'app5/list_transaksi_makan.html', response)

def delete_transaksi_makan(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)

    # hanya admin_satgas yang bisa melakukan operasi delete
    response = {}
    set_username(request, response)

    if (request.session.get('peran') not in [ADMIN_SATGAS]):
        return render(request, ERROR_TEMPLATE, response)
    
    if (request.method == 'POST'):
        try:
            # cek apakah transaksi hotel terkait sudah lunas
            # jika sudah, batalkan operasi
            stmt = '''
            SELECT th.statusbayar
            FROM TRANSAKSI_HOTEL th
            JOIN TRANSAKSI_MAKAN tm
            ON th.idtransaksi = %s
            WHERE tm.idtransaksimakan = %s
            '''
            with connection.cursor() as cursor:
                cursor.execute(stmt, [request.POST['idTransaksi'], request.POST['idTransaksiMakan']])
                status_bayar = cursor.fetchone()[0]
            if (status_bayar == 'LUNAS'):
                return render(request, ERROR_TEMPLATE, response)

            # delete daftar pesan
            stmt = '''
            DELETE FROM DAFTAR_PESAN
            WHERE id_transaksi = %s
                AND idtransaksimakan = %s;
            '''
            with connection.cursor() as cursor:
                cursor.execute(stmt, [request.POST['idTransaksi'], request.POST['idTransaksiMakan']])
                connection.commit()

            # delete transaksi makan
            stmt = '''
            DELETE FROM TRANSAKSI_MAKAN
            WHERE idtransaksi = %s
                AND idtransaksimakan = %s;
            '''
            with connection.cursor() as cursor:
                cursor.execute(stmt, [request.POST['idTransaksi'], request.POST['idTransaksiMakan']])
                connection.commit()

        except:
            pass

    return redirect(url_list_transaksi_makan)

def detail_transaksi_makan(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    
    response = {}
    set_username(request, response)

    # hanya admin satgas dan pengguna publik yang dapat melakukan operasi ini
    if (request.session.get('peran') not in [ADMIN_SATGAS, PENGGUNA_PUBLIK]):
        return render(request, ERROR_TEMPLATE, response)

    # kalau tidak diberi parameter, langsung 404
    try:
        response['idTransaksi'] = request.GET['idTransaksi']
        response['idTransaksiMakan'] = request.GET['idTransaksiMakan']
    except:
        raise Http404

    if (request.session.get('peran') == PENGGUNA_PUBLIK):
        # jika yang membaca pengguna publik, maka 
        # dia hanya bisa membaca transaksi yang dibuatnya
        # cek apakah boleh dibaca, jika tidak, alihkan ke 404
        # sekaligus dapatkan total bayar transaksi makan
        stmt = '''
        SELECT tm.totalbayar
        FROM TRANSAKSI_MAKAN tm
        JOIN TRANSAKSI_HOTEL th
        ON th.idtransaksi = tm.idtransaksi
        JOIN PASIEN p
        ON p.nik = th.kodepasien
        WHERE p.idpendaftar = %s
            AND tm.idtransaksi = %s
            AND tm.idtransaksimakan = %s;
        '''
        total_bayar = None
        try:
            with connection.cursor() as cursor:
                cursor.execute(stmt, [request.session.get('username'), request.GET['idTransaksi'], request.GET['idTransaksiMakan']])
                total_bayar = cursor.fetchone()[0]
        except:
            pass        
    else:
        # jika yang membaca admin satgas, maka
        # dia bisa membaca seluruh transaksi makan
        stmt = '''
        SELECT tm.totalbayar
        FROM TRANSAKSI_MAKAN tm
        WHERE tm.idtransaksi = %s
            AND tm.idtransaksimakan = %s;
        '''
        total_bayar = None
        try:
            with connection.cursor() as cursor:
                cursor.execute(stmt, [request.GET['idTransaksi'], request.GET['idTransaksiMakan']])
                total_bayar = cursor.fetchone()[0]
        except:
            pass
        
    if (total_bayar == None):
        raise Http404
    response['totalBayar'] = total_bayar

    # kode hotel
    stmt = '''
    SELECT rh.kodehotel
    FROM RESERVASI_HOTEL rh, TRANSAKSI_BOOKING tb
    WHERE tb.idtransaksibooking = %s
        AND tb.kodepasien = rh.kodepasien
        AND tb.tglmasuk = rh.tglmasuk;
    '''
    try:
        with connection.cursor() as cursor:
            cursor.execute(stmt, [request.GET['idTransaksi']])
            response['kodeHotel'] = cursor.fetchone()[0]
    except:
        pass

    # daftar pesan
    stmt = '''
    SELECT dp.id_pesanan, dp.kodepaket, pm.harga
    FROM DAFTAR_PESAN dp, PAKET_MAKAN pm
    WHERE
        dp.kodepaket = pm.kodepaket AND
        dp.id_transaksi = %s AND
        dp.idtransaksimakan = %s
    '''
    try:
        with connection.cursor() as cursor:
            cursor.execute(stmt, [request.GET['idTransaksi'], request.GET['idTransaksiMakan']])
            response['daftarPesan'] = cursor.fetchall()
    except:
        pass

    return render(request, 'app5/detail_transaksi_makan.html', response)

def update_transaksi_makan(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
        
    response = {}
    set_username(request, response)

    # hanya admin satgas yang dapat melakukan operasi ini
    if (request.session.get('peran') not in [ADMIN_SATGAS]):
        return render(request, ERROR_TEMPLATE, response)

    if (request.method == 'POST'):
        try:
            id_transaksi = request.POST['idTransaksi']
            id_transaksi_makan = request.POST['idTransaksiMakan']
            kode_hotel = request.POST['kodeHotel']
            kode_paket = request.POST.getlist('kodePaket')

            # memasukkan semua kode paket ke daftar pesan
            stmt = '''
            INSERT INTO DAFTAR_PESAN(idtransaksimakan, id_transaksi, kodehotel, kodepaket)
            VALUES (%s, %s, %s, %s)
            '''
            for i in range(len(kode_paket)):
                with connection.cursor() as cursor:
                    cursor.execute(stmt, [id_transaksi_makan, id_transaksi, kode_hotel, kode_paket[i]])
                    connection.commit()

            return redirect(url_list_transaksi_makan)
        except:
            response['error'] = 'Terjadi suatu kesalahan, silakan coba lagi nanti.'
    
    # kalau tidak diberi parameter, langsung 404
    try:
        response['idTransaksi'] = request.GET['idTransaksi']
        response['idTransaksiMakan'] = request.GET['idTransaksiMakan']
    except:
        raise Http404

    # kode hotel
    stmt = '''
    SELECT rh.kodehotel
    FROM RESERVASI_HOTEL rh, TRANSAKSI_BOOKING tb
    WHERE tb.idtransaksibooking = %s
        AND tb.kodepasien = rh.kodepasien
        AND tb.tglmasuk = rh.tglmasuk;
    '''
    try:
        with connection.cursor() as cursor:
            cursor.execute(stmt, [request.GET['idTransaksi']])
            response['kodeHotel'] = cursor.fetchone()[0]
    except:
        pass

    # daftar pesan
    stmt = '''
    SELECT dp.kodepaket
    FROM DAFTAR_PESAN dp
    WHERE
        dp.id_transaksi = %s AND
        dp.idtransaksimakan = %s
    '''
    try:
        with connection.cursor() as cursor:
            cursor.execute(stmt, [request.GET['idTransaksi'], request.GET['idTransaksiMakan']])
            response['daftarPesan'] = [x[0] for x in cursor.fetchall()]
    except:
        pass

    return render(request, 'app5/update_transaksi_makan.html', response)
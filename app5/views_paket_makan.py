from django.shortcuts import render, redirect
from django.db import connection
from .forms import *
from django.http import JsonResponse, Http404

url_dashboard = 'app1:dashboard'
url_login = 'app1:login'
url_list_paket_makan = 'app5:listPaketMakan'
ERROR_TEMPLATE = 'app5/error.html'
ADMIN_SATGAS = 'admin_satgas'
PENGGUNA_PUBLIK = 'pengguna_publik'
ADMIN = 'admin'

def set_username(request, response):
    response['is_authenticated'] = request.session.get('is_authenticated')
    response['username'] = request.session.get('username')
    response['peran'] = request.session.get('peran')

def create_paket_makan(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)

    response = {}
    set_username(request, response)

    # hanya admin yang dapat melakukan operasi ini
    if (request.session.get('peran') not in [ADMIN]):
        return render(request, ERROR_TEMPLATE, response)

    if (request.method == 'POST'):
        form = PaketMakanForm(request.POST)
        if (form.is_valid()):
            stmt = '''
            INSERT INTO PAKET_MAKAN(kodehotel, kodepaket, nama, harga)
            VALUES
                (%s, %s, %s, %s)
            '''
            try:
                with connection.cursor() as cursor:
                    cursor.execute(stmt, [
                        form.cleaned_data['kode_hotel'],
                        form.cleaned_data['kode_paket'],
                        form.cleaned_data['nama'],
                        form.cleaned_data['harga'],
                    ])
                    connection.commit()
                return redirect(url_list_paket_makan)
            except:
                response['error'] = 'Terjadi suatu kesalahan, silakan coba lagi nanti.'    
        else:
            response['error'] = 'Terjadi suatu kesalahan, silakan coba lagi nanti.'

    stmt = '''
    SELECT kode FROM HOTEL;
    '''
    kode_hotel = tuple()
    try:
        with connection.cursor() as cursor:
            cursor.execute(stmt)
            kode_hotel = cursor.fetchall()
    except:
        pass
    response['kode_hotel'] = [kode[0] for kode in kode_hotel]

    return render(request, 'app5/create_paket_makan.html', response)

def list_paket_makan(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)

    response = {}
    set_username(request, response)

    if (request.session.get('peran') not in [ADMIN, ADMIN_SATGAS, PENGGUNA_PUBLIK]):
        return render(request, ERROR_TEMPLATE, response)

    stmt = '''
    SELECT pm.kodehotel, pm.kodepaket, pm.nama, pm.harga, count(dp.kodepaket)
    FROM PAKET_MAKAN pm
    LEFT JOIN DAFTAR_PESAN dp
    ON pm.kodepaket = dp.kodepaket
    GROUP BY (pm.kodehotel, pm.kodepaket);
    '''
    try:
        with connection.cursor() as cursor:
            cursor.execute(stmt)
            response['paket_makan'] = cursor.fetchall()
    except:
        pass

    return render(request, 'app5/list_paket_makan.html', response)

def delete_paket_makan(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)

    response = {}
    set_username(request, response)

    if (request.session.get('peran') not in [ADMIN]):
        return render(request, ERROR_TEMPLATE, response)

    if (request.method == 'POST'):
        # cek apakah paket makan terkait bisa dihapus
        # dengan cek banyak daftar pesan yang berisi kodepaket tersebut
        stmt = '''
        SELECT count(kodepaket)
        FROM DAFTAR_PESAN
        WHERE kodehotel = %s AND
            kodepaket = %s;
        '''
        try:
            with connection.cursor() as cursor:
                cursor.execute(stmt, [request.POST['kode_hotel'], request.POST['kode_paket']])
                banyak_daftar_pesan = cursor.fetchone()[0]
            if (banyak_daftar_pesan != 0):
                return render(request, ERROR_TEMPLATE, response)

            # delete dari tabel paket makan
            stmt = '''
            DELETE FROM PAKET_MAKAN
            WHERE kodehotel = %s AND
                kodepaket = %s;
            '''
            with connection.cursor() as cursor:
                cursor.execute(stmt, [request.POST['kode_hotel'], request.POST['kode_paket']])
                connection.commit()
        except:
            pass

    return redirect(url_list_paket_makan)

def update_paket_makan(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)

    response = {}
    set_username(request, response)

    if (request.session.get('peran') not in [ADMIN]):
        return render(request, ERROR_TEMPLATE, response)

    if (request.method == 'POST'):
        try:
            form = PaketMakanForm(request.POST)
            if (form.is_valid()):
                stmt = '''
                UPDATE PAKET_MAKAN
                SET
                    nama = %s,
                    harga = %s
                WHERE
                    kodehotel = %s AND
                    kodepaket = %s;
                '''
                with connection.cursor() as cursor:
                    cursor.execute(stmt, [
                        form.cleaned_data['nama'],
                        form.cleaned_data['harga'],
                        form.cleaned_data['kode_hotel'],
                        form.cleaned_data['kode_paket'],
                    ])
                    connection.commit()
                return redirect(url_list_paket_makan)
            else:
                response['error'] = 'Terjadi suatu kesalahan, silakan coba lagi nanti.'
        except:
            response['error'] = 'Terjadi suatu kesalahan, silakan coba lagi nanti.'

    # kalau tidak diberi parameter, langsung 404
    try:
        response['kode_hotel'] = request.GET['kode_hotel']
        response['kode_paket'] = request.GET['kode_paket']
    except:
        raise Http404

    stmt = '''
    SELECT nama, harga
    FROM paket_makan
    WHERE kodehotel = %s
        AND kodepaket = %s
    '''
    try:
        with connection.cursor() as cursor:
            cursor.execute(stmt, [request.GET['kode_hotel'], request.GET['kode_paket']])
            response['paket_makan'] = cursor.fetchone()
    except:
        pass

    return render(request, 'app5/update_paket_makan.html', response)

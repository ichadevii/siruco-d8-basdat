from django.urls import re_path, path
from .views_transaksi_makan_dan_daftar_pesan import *
from .views_paket_makan import *
from .views_hotel import *

app_name = 'app5'

urlpatterns = [
    # transaksi makan dan daftar pesan
    path('createTransaksiMakan/', create_transaksi_makan, name='createTransaksiMakan'),
    path('listTransaksiMakan/', list_transaksi_makan, name='listTransaksiMakan'),
    path('deleteTransaksiMakan/', delete_transaksi_makan, name='deleteTransaksiMakan'),
    path('updateTransaksiMakan/', update_transaksi_makan, name='updateTransaksiMakan'),
    path('detailTransaksiMakan/', detail_transaksi_makan, name='detailTransaksiMakan'),
    path('api/getKodeHotelFromIdTransaksi', get_kode_hotel_from_id_transaksi, name="api_getKodeHotelFromIdTransaksi"),
    path('api/getKodePaketFromIdTransaksi', get_kode_paket_from_id_transaksi, name="api_getKodePaketFromIdTransaksi"),

    # paket makan
    path('createPaketMakan/', create_paket_makan, name='createPaketMakan'),
    path('listPaketMakan/', list_paket_makan, name='listPaketMakan'),
    path('deletePaketMakan/', delete_paket_makan, name='deletePaketMakan'),
    path('updatePaketMakan/', update_paket_makan, name='updatePaketMakan'),

    # hotel
    path('createHotel/', create_hotel, name='createHotel'),
    path('listHotel/', list_hotel, name='listHotel'),
    path('updateHotel/', update_hotel, name='updateHotel'),
]
from django.shortcuts import render, redirect
from django.db import connection
from .forms import *
from django.http import JsonResponse, Http404

url_dashboard = 'app1:dashboard'
url_login = 'app1:login'
url_list_hotel = 'app5:listHotel'
ERROR_TEMPLATE = 'app5/error.html'
ADMIN_SATGAS = 'admin_satgas'
PENGGUNA_PUBLIK = 'pengguna_publik'
ADMIN = 'admin'

def set_username(request, response):
    response['is_authenticated'] = request.session.get('is_authenticated')
    response['username'] = request.session.get('username')
    response['peran'] = request.session.get('peran')

def create_hotel(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)

    response = {}
    set_username(request, response)

    if (request.session.get('peran') not in [ADMIN]):
        return render(request, ERROR_TEMPLATE, response)

    if (request.method == 'POST'):
        form = HotelForm(request.POST)
        if (form.is_valid()):
            try:
                stmt = '''
                INSERT INTO HOTEL(kode, nama, isrujukan, jalan, kelurahan, kecamatan, kabkot, prov)
                VALUES
                    (%s,%s,%s,%s,%s,%s,%s,%s);
                '''
                is_rujukan = '0'
                if (form.cleaned_data['is_rujukan']):
                    is_rujukan = '1'
                with connection.cursor() as cursor:
                    cursor.execute(stmt,[
                        request.POST['kode_hotel'],
                        form.cleaned_data['nama'],
                        is_rujukan,
                        form.cleaned_data['jalan'],
                        form.cleaned_data['kelurahan'],
                        form.cleaned_data['kecamatan'],
                        form.cleaned_data['kabkot'],
                        form.cleaned_data['prov'],
                    ])
                    connection.commit()
                return redirect(url_list_hotel)
            except:
                response['error'] = 'Terjadi suatu kesalahan, silakan coba lagi nanti.'        
        else:
            response['error'] = 'Terjadi suatu kesalahan, silakan coba lagi nanti.'    
    
    # mendapatkan kode_hotel
    stmt = '''
    SELECT kode FROM HOTEL
    ORDER BY kode DESC
    LIMIT 1;
    '''
    try:
        with connection.cursor() as cursor:
            cursor.execute(stmt)
            nomor_kode_hotel = cursor.fetchall()
    except:
        pass
    
    if (len(nomor_kode_hotel) == 0):
        nomor_kode_hotel = 0
    else:
        nomor_kode_hotel = int(nomor_kode_hotel[0][0][1:])
    response['kode_hotel'] = 'H' + str(nomor_kode_hotel + 1).zfill(4)
    return render(request, 'app5/create_hotel.html', response)

def list_hotel(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)

    response = {}
    set_username(request, response)

    if (request.session.get('peran') not in [ADMIN, ADMIN_SATGAS, PENGGUNA_PUBLIK]):
        return render(request, ERROR_TEMPLATE, response)

    stmt = '''
    SELECT kode, nama, isrujukan, jalan, kelurahan, kecamatan, kabkot, prov
    FROM HOTEL; 
    '''
    try:
        with connection.cursor() as cursor:
            cursor.execute(stmt)
            response['hotel'] = cursor.fetchall()
    except:
        pass    

    return render(request, 'app5/list_hotel.html', response)

def update_hotel(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)

    response = {}
    set_username(request, response)

    if (request.session.get('peran') not in [ADMIN]):
        return render(request, ERROR_TEMPLATE, response)

    if (request.method == 'POST'):
        form = HotelForm(request.POST)
        if (form.is_valid()):
            try:
                stmt = '''
                UPDATE HOTEL SET
                    nama=%s,
                    isrujukan=%s,
                    jalan=%s,
                    kelurahan=%s,
                    kecamatan=%s,
                    kabkot=%s,
                    prov=%s
                WHERE kode=%s;
                '''
                is_rujukan = '0'
                if (form.cleaned_data['is_rujukan']):
                    is_rujukan = '1'
                with connection.cursor() as cursor:
                    cursor.execute(stmt,[
                        form.cleaned_data['nama'],
                        is_rujukan,
                        form.cleaned_data['jalan'],
                        form.cleaned_data['kelurahan'],
                        form.cleaned_data['kecamatan'],
                        form.cleaned_data['kabkot'],
                        form.cleaned_data['prov'],
                        request.POST['kode_hotel'],
                    ])
                    connection.commit()
                return redirect(url_list_hotel)
            except:
                response['error'] = 'Terjadi suatu kesalahan, silakan coba lagi nanti.'        
        else:
            response['error'] = 'Terjadi suatu kesalahan, silakan coba lagi nanti.'

    # kalau tidak diberi parameter, langsung 404
    try:
        response['kode_hotel'] = request.GET['kode_hotel']
    except:
        raise Http404

    stmt = '''
    SELECT nama, isrujukan, jalan, kelurahan, kecamatan, kabkot, prov
    FROM HOTEL
    WHERE kode = %s;
    '''
    try:
        with connection.cursor() as cursor:
            cursor.execute(stmt, [request.GET['kode_hotel']])
            response['hotel'] = cursor.fetchone()
    except:
        pass

    return render(request, 'app5/update_hotel.html', response)
from django import forms

class CreateRuanganHotelForm(forms.Form):

    kode_hotel = forms.CharField(
        max_length=5,
        min_length=3
    )

    kode_ruangan = forms.CharField(
        max_length=5,
        min_length=5
    )

    jenis_bed = forms.CharField(
        max_length=10
    )

    tipe = forms.CharField(
        max_length=10
    )

    harga = forms.IntegerField(
        min_value=0
    )

class CreateReservasiHotelForm(forms.Form):

    nik_pasien = forms.CharField(
        max_length=20
    )

    tgl_masuk = forms.DateField()

    tgl_keluar = forms.DateField()

    kode_hotel = forms.CharField(
        max_length=5,
        min_length=3
    )

    kode_ruangan = forms.CharField(
        max_length=5,
        min_length=3
    )

class UpdateTransaksiHotelForm(forms.Form):
    status = forms.CharField(
        max_length=15
    )
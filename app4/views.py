from django.shortcuts import redirect, render
from django.db import connection
from json import dumps
from .forms import *
import datetime
from django.http import JsonResponse

url_dashboard = 'app1:dashboard'
url_login = 'app1:login'
ADMIN_SATGAS = 'admin_satgas'
PENGGUNA_PUBLIK = 'pengguna_publik'
ADMIN = 'admin'

#set DATABASE_URL=postgresql://postgres:080620@localhost:5432/siruco

def set_up(request, response):
    response['is_authenticated'] = request.session.get('is_authenticated')
    response['username'] = request.session.get('username')
    response['peran'] = request.session.get('peran')


### CRUD RUANGAN HOTEL ###
 
def listRuanganHotel(request):

    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)

    response = {}
    set_up(request, response)

    if (request.session.get('peran') not in [ADMIN, ADMIN_SATGAS, PENGGUNA_PUBLIK]):
        return redirect(url_dashboard)

    stmt = '''
    SELECT HOTEL_ROOM.kodehotel, HOTEL_ROOM.koderoom, jenisbed, tipe, harga, MAX(tglkeluar)
    FROM HOTEL_ROOM LEFT OUTER JOIN RESERVASI_HOTEL
    ON HOTEL_ROOM.kodehotel = RESERVASI_HOTEL.kodehotel
    AND HOTEL_ROOM.koderoom = RESERVASI_HOTEL.koderoom
    AND RESERVASI_HOTEL.tglkeluar > CURRENT_DATE
    GROUP by HOTEL_ROOM.kodehotel, HOTEL_ROOM.koderoom
    ORDER by kodehotel, koderoom ASC; 
    '''

    try:
        with connection.cursor() as cursor:
            cursor.execute(stmt)
            response['ruangan_hotel'] = cursor.fetchall()
    except:
        pass

    return render(request, 'app4/listRuanganHotel.html', response)


def deleteRuanganHotel(request, kode_hotel, kode_ruangan):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)

    response = {}
    set_up(request, response)

    if (request.session.get('peran') not in [ADMIN]):
        return redirect(url_dashboard)

    stmt = '''
    DELETE FROM HOTEL_ROOM
    WHERE kodehotel = (%s) AND koderoom = (%s);
    '''

    try:
        with connection.cursor() as cursor:
            cursor.execute(stmt, 
            [kode_hotel, kode_ruangan,])

        connection.commit()
            
        return redirect('/hotel/ruangan/')
    except Exception as e:
        e = str(e)
        print(e)


def createRuanganHotel(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)

    response = {}
    set_up(request, response)

    if (request.session.get('peran') not in [ADMIN]):
        return redirect(url_dashboard)

    if (request.method == 'POST'):
        print(request.POST)
        form = CreateRuanganHotelForm(request.POST)
        if (form.is_valid()):
            stmt = '''
            INSERT INTO HOTEL_ROOM(kodehotel, koderoom, jenisbed, tipe, harga) 
            VALUES (%s, %s, %s, %s, %s);
            '''

            try:
                with connection.cursor() as cursor:
                    cursor.execute(stmt, 
                    [form.cleaned_data['kode_hotel'], 
                    form.cleaned_data['kode_ruangan'], 
                    form.cleaned_data['jenis_bed'],
                    form.cleaned_data['tipe'],
                    form.cleaned_data['harga'],
                    ])

                connection.commit()
                    
                return redirect('/hotel/ruangan/')
            except Exception as e:
                e = str(e)
                print(e)
    
    return render(request, 'app4/createRuanganHotel.html', response)


def createRuanganHotelAsyc(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)

    response = {}
    set_up(request, response)

    if (request.session.get('peran') not in [ADMIN]):
        return redirect(url_dashboard)

    response = {}

    stmt1 = '''
    SELECT kode, MAX(koderoom)
    FROM HOTEL
    LEFT OUTER JOIN HOTEL_ROOM
    ON HOTEL.kode = HOTEL_ROOM.kodehotel
    GROUP by kode
    ORDER by kode ASC;
    '''

    stmt2 = '''
    SELECT kode
    FROM HOTEL
    ORDER by kode ASC;
    '''
    try:
        with connection.cursor() as cursor:
            cursor.execute(stmt1)
            response['ruangan_hotel'] = cursor.fetchall()
        with connection.cursor() as cursor:
            cursor.execute(stmt2)
            response['kode_hotel'] = cursor.fetchall()
    except:
        pass

    return JsonResponse({'data': response})


def updateRuanganHotel(request, kode_hotel, kode_ruangan):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)

    response = {}
    set_up(request, response)

    if (request.session.get('peran') not in [ADMIN]):
        return redirect(url_dashboard)

    k_hotel = str(kode_hotel)
    k_ruangan = str(kode_ruangan)

    if (request.method == 'POST'):
        print(request.POST)
        form = CreateRuanganHotelForm(request.POST)
        if (form.is_valid()):
            stmt = '''
            UPDATE HOTEL_ROOM
            SET jenisbed = (%s),
                tipe = (%s),
                harga = (%s)
            WHERE kodehotel = (%s) AND koderoom = (%s);
            '''

            try:
                with connection.cursor() as cursor:
                    cursor.execute(stmt, 
                    [form.cleaned_data['jenis_bed'],
                    form.cleaned_data['tipe'],
                    form.cleaned_data['harga'],
                    k_hotel, k_ruangan,
                    ])

                connection.commit()
                    
                return redirect('/hotel/ruangan/')
            except Exception as e:
                e = str(e)
                print(e)

    stmt = '''
    SELECT kodehotel, koderoom, jenisbed, tipe, harga
    FROM HOTEL_ROOM
    WHERE kodehotel = (%s) AND koderoom = (%s); 
    '''
    try:
        with connection.cursor() as cursor:
            cursor.execute(stmt, [k_hotel, k_ruangan,])
            response['details'] = cursor.fetchall()
    except:
        pass

    return render(request, 'app4/updateRuanganHotel.html', response)



### CRUD RESERVASI HOTEL ###

def listReservasiHotel(request):

    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)

    response = {}
    set_up(request, response)

    if (request.session.get('peran') not in [ADMIN_SATGAS, PENGGUNA_PUBLIK]):
        return redirect(url_dashboard)

    stmt = '''
    SELECT DISTINCT R1.kodepasien, R1.tglmasuk, R1.tglkeluar, R1.kodehotel, R1.koderoom, R2.tglmasuk
    FROM RESERVASI_HOTEL R1 LEFT OUTER JOIN RESERVASI_HOTEL R2
    ON R1.kodepasien = R2.kodepasien
    AND R1.tglmasuk = R2.tglmasuk
    AND R2.tglmasuk > CURRENT_DATE;
    '''
    try:
        with connection.cursor() as cursor:
            cursor.execute(stmt)
            response['reservasi_hotel'] = cursor.fetchall()
    except:
        pass    

    # print(response)

    return render(request, 'app4/listReservasiHotel.html', response)


def deleteReservasiHotel(request, kode_pasien, tgl_masuk):

    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)

    response = {}
    set_up(request, response)

    if (request.session.get('peran') not in [ADMIN_SATGAS]):
        return redirect(url_dashboard)

    stmt = '''
    DELETE FROM RESERVASI_HOTEL
    WHERE kodepasien = (%s) AND tglmasuk = (%s);
    '''

    try:
        with connection.cursor() as cursor:
            cursor.execute(stmt, 
            [kode_pasien, tgl_masuk,])

        connection.commit()
            
        return redirect('/hotel/reservasi/')
    except Exception as e:
        e = str(e)
        print(e)


def createReservasiHotel(request):

    if (not(request.session.get('is_authenticated'))):
        return redirect(url_dashboard)

    response = {}
    set_up(request, response)

    if (request.session.get('peran') not in [ADMIN_SATGAS, PENGGUNA_PUBLIK]):
        return render(request, url_dashboard, response)

    if (request.method == 'POST'):
        print(request.POST)
        form = CreateReservasiHotelForm(request.POST)
        if (form.is_valid()):
            stmt = '''
            INSERT INTO RESERVASI_HOTEL(kodepasien, tglmasuk, tglkeluar, kodehotel, koderoom) 
            VALUES (%s, %s, %s, %s, %s);
            '''

            try:
                with connection.cursor() as cursor:
                    cursor.execute(stmt, 
                    [form.cleaned_data['nik_pasien'], 
                    form.cleaned_data['tgl_masuk'], 
                    form.cleaned_data['tgl_keluar'],
                    form.cleaned_data['kode_hotel'],
                    form.cleaned_data['kode_ruangan'],
                    ])

                connection.commit()
                    
                return redirect('/hotel/reservasi')
            except Exception as e:
                e = str(e)
                print(e)

    stmt1 = '''
    SELECT kode
    FROM HOTEL
    ORDER by kode ASC;
    '''

    try:
        with connection.cursor() as cursor:
            cursor.execute(stmt1)
            response['kode_hotel'] = cursor.fetchall()
    except:
        pass    

    if (request.session.get('peran') == ADMIN_SATGAS):
        stmt3 = '''
        SELECT nik
        FROM PASIEN;
        '''

        try:
            with connection.cursor() as cursor:
                cursor.execute(stmt3)
                response['nik_pasien'] = cursor.fetchall()
        except:
            pass

    if (request.session.get('peran') == PENGGUNA_PUBLIK):
        stmt3 = '''
        SELECT nik
        FROM PASIEN
        WHERE idpendaftar = (%s);
        '''

        try:
            with connection.cursor() as cursor:
                cursor.execute(stmt3, [request.session.get('username')])
                response['nik_pasien'] = cursor.fetchall()
        except:
            pass
    
    return render(request, 'app4/createReservasiHotel.html', response)


def createReservasiHotelAsyc(request, kode_hotel):
    
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_dashboard)

    response = {}
    set_up(request, response)

    if (request.session.get('peran') not in [ADMIN_SATGAS, PENGGUNA_PUBLIK]):
        return render(request, url_dashboard, response)
    
    stmt1 = '''
    SELECT koderoom
    FROM HOTEL_ROOM
    WHERE kodehotel = (%s)
    ORDER by koderoom ASC; 
    '''

    hotel = str(kode_hotel)

    try:
        with connection.cursor() as cursor:
            cursor.execute(stmt1, [hotel])
            response['kode_ruangan'] = cursor.fetchall()
    except Exception as e:
        e = str(e)
        print(e)

    return JsonResponse({'data': response})


def updateReservasiHotel(request, kode_pasien, tgl_masuk):

    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)

    response = {}
    set_up(request, response)

    if (request.session.get('peran') not in [ADMIN_SATGAS]):
        return redirect(url_dashboard)

    k_pasien = str(kode_pasien)
    t_masuk = str(tgl_masuk)

    if (request.method == 'POST'):
        print(request.POST)
        form = CreateReservasiHotelForm(request.POST)
        if (form.is_valid()):
            stmt = '''
            UPDATE RESERVASI_HOTEL
            SET tglkeluar = (%s)
            WHERE kodepasien = (%s) AND tglmasuk = (%s);
            '''

            try:
                with connection.cursor() as cursor:
                    cursor.execute(stmt, 
                    [form.cleaned_data['tgl_keluar'],
                    k_pasien, t_masuk,
                    ])

                connection.commit()
                    
                return redirect('/hotel/reservasi/')
            except Exception as e:
                e = str(e)
                print(e)

    stmt = '''
    SELECT kodepasien, tglmasuk, tglkeluar, kodehotel, koderoom
    FROM RESERVASI_HOTEL
    WHERE kodepasien = (%s) AND tglmasuk = (%s); 
    '''
    try:
        with connection.cursor() as cursor:
            cursor.execute(stmt, [k_pasien, t_masuk,])
            response['details'] = cursor.fetchall()
    except:
        pass    

    # print(response)

    return render(request, 'app4/updateReservasiHotel.html', response)



### CRUD TRANSAKSI HOTEL ###

def listTransaksiHotel(request):

    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)

    response = {}
    set_up(request, response)

    if (request.session.get('peran') not in [ADMIN_SATGAS]):
        return redirect(url_dashboard)

    stmt = '''
    SELECT *
    FROM TRANSAKSI_HOTEL; 
    '''
    try:
        with connection.cursor() as cursor:
            cursor.execute(stmt)
            response['transaksi_hotel'] = cursor.fetchall()
    except:
        pass    

    # print(response)

    # response['waktu_bayar'] = str(response['details'][0][3]).split()[1]

    print(response['transaksi_hotel'])

    for i in range(len(response['transaksi_hotel'])):
        x = response['transaksi_hotel'][i]
        y = list(x)
        try:
            y[3] = str(x[3]).split()[1]
        except:
            pass
        x = tuple(y)
        response['transaksi_hotel'][i] = x


    return render(request, 'app4/listTransaksiHotel.html', response)


def updateTransaksiHotel(request, id_transaksi):

    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)

    response = {}
    set_up(request, response)

    if (request.session.get('peran') not in [ADMIN_SATGAS]):
        return redirect(url_dashboard)

    if (request.method == 'POST'):
        print(request.POST)
        form = UpdateTransaksiHotelForm(request.POST)
        if (form.is_valid()):
            print('titit')
            stmt = '''
            UPDATE TRANSAKSI_HOTEL
            SET statusbayar = (%s)
            WHERE idtransaksi = (%s);
            '''

            try:
                with connection.cursor() as cursor:
                    cursor.execute(stmt, 
                    [form.cleaned_data['status'], id_transaksi,])

                connection.commit()
                    
                return redirect('/hotel/transaksi/')
            except Exception as e:
                e = str(e)
                print(e)

    stmt = '''
    SELECT *
    FROM TRANSAKSI_HOTEL
    WHERE idtransaksi = (%s); 
    '''
    try:
        with connection.cursor() as cursor:
            cursor.execute(stmt, [id_transaksi])
            response['details'] = cursor.fetchall()
    except:
        pass    

    # print(response)
    
    x = response['details'][0]
    y = list(x)
    try:
        y[3] = str(x[3]).split()[1]
    except:
        pass
    x = tuple(y)
    response['details'][0] = x

    return render(request, 'app4/updateTransaksiHotel.html', response)



### CRUD TRANSAKSI BOOKING ###

def listTransaksiBooking(request):

    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)

    response = {}
    set_up(request, response)

    # if (request.session.get('peran') not in [ADMIN_SATGAS, PENGGUNA_PUBLIK]):
    #     return redirect(url_dashboard)

    stmt = '''
    SELECT idtransaksibooking, totalbayar, kodepasien, tglmasuk
    FROM TRANSAKSI_BOOKING; 
    '''
    try:
        with connection.cursor() as cursor:
            cursor.execute(stmt)
            response['transaksi_booking'] = cursor.fetchall()
    except:
        pass    

    # print(response)
    
    return render(request, 'app4/listTransaksiBooking.html', response)
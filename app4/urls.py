from django.urls import re_path, path
from .views import *

app_name = 'app4'

urlpatterns = [
    path('hotel/ruangan/', listRuanganHotel, name='listRuanganHotel'),
    path('hotel/ruangan/create/', createRuanganHotel, name='createRuanganHotel'),
    path('hotel/ruangan/create/asyc/', createRuanganHotelAsyc, name='createRuanganHotelAsyc'),
    path('hotel/ruangan/delete/<str:kode_hotel>/<str:kode_ruangan>/', deleteRuanganHotel, name='deleteRuanganHotel'),
    path('hotel/ruangan/update/<str:kode_hotel>/<str:kode_ruangan>/', updateRuanganHotel, name='updateRuanganHotel'),
    path('hotel/reservasi/', listReservasiHotel, name='listReservasiHotel'),
    path('hotel/reservasi/create/', createReservasiHotel, name='createReservasiHotel'),
    path('hotel/reservasi/create/asyc/<str:kode_hotel>/', createReservasiHotelAsyc, name='createReservasiHotelAsyc'),
    path('hotel/reservasi/update/<str:kode_pasien>/<str:tgl_masuk>/', updateReservasiHotel, name='updateReservasiHotel'),
    path('hotel/reservasi/delete/<str:kode_pasien>/<str:tgl_masuk>/', deleteReservasiHotel, name='deleteReservasiHotel'),
    path('hotel/transaksi/', listTransaksiHotel, name='listTransaksiHotel'),
    path('hotel/transaksi/update/<str:id_transaksi>/', updateTransaksiHotel, name='updateTransaksiHotel'),
    path('hotel/transaksi/booking/', listTransaksiBooking, name='listTransaksiBooking'),
]
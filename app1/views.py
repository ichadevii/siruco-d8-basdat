from django.shortcuts import render, redirect
from django.db import connection
from django.shortcuts import render
from .forms import *
from django.http import JsonResponse, Http404

url_dashboard = 'app1:dashboard'
url_login = 'app1:login'
url_listPasien = 'app1:listPasien'

def set_username(request, response):
    response['is_authenticated'] = request.session.get('is_authenticated')
    response['username'] = request.session.get('username')
    response['peran'] = request.session.get('peran')

def dashboard(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)
    return render(request, 'app1/dashboard.html', response)

def homepage(request):
    if (request.session.get('is_authenticated')):
        return redirect(url_dashboard)
    response = {}
    set_username(request, response)
    return render(request, 'app1/homepage.html', response)

def login(request):
    if (request.session.get('is_authenticated')):
        return redirect(url_dashboard)
    response = {}

    if (request.method == 'POST'):
        form = LoginForm(request.POST)
        if form.is_valid():
            stmt = '''
            SELECT * FROM akun_pengguna WHERE username=%s AND PASSWORD=%s;
            '''
            try:
                user = tuple()
                with connection.cursor() as cursor:
                    cursor.execute(stmt, [form.cleaned_data['email'], form.cleaned_data['password']])
                    user = cursor.fetchone()
                request.session['username'] = user[0]
                request.session['peran'] = user[2]
                request.session['is_authenticated'] = True
                return redirect(url_dashboard)
            except:
                response['error'] = 'Email atau password salah, silakan coba lagi'

    form = LoginForm(request.POST)
    response['form'] = form
    return render(request, 'app1/login.html', response)

def logout(request):
    request.session.flush()
    return redirect(url_login)

def registerAdminSistem(request):
    if (request.session.get('is_authenticated')):
        return redirect(url_dashboard)
    peran = 'admin'
    response = {}

    if (request.method == 'POST'):
        form = RegisterAdminSistemForm(request.POST)    
        if (form.is_valid()):
            stmt1 = '''
            INSERT INTO akun_pengguna(username, password, peran) VALUES (%s,%s,%s);
            '''

            stmt2 = '''
            INSERT INTO admin(username) VALUES (%s);
            '''
            try:
                with connection.cursor() as cursor:
                    cursor.execute(stmt1, [form.cleaned_data['email'], form.cleaned_data['password'], peran])
                    connection.commit()
                with connection.cursor() as cursor:
                    cursor.execute(stmt2, [form.cleaned_data['email']])
                    connection.commit()
                
                request.session['username'] = form.cleaned_data['email']
                request.session['peran'] = peran
                request.session['is_authenticated'] = True
                return redirect(url_dashboard)
            except Exception as e:
                e = str(e)
                if ('already exists' in e):
                    response['error'] = 'Email telah digunakan, silakan gunakan email lain'
                elif ('Password harus memiliki minimal satu huruf kapital dan satu angka.' in e):
                    response['error'] = 'Password Anda belum memenuhi syarat, silahkan pastikan bahwa password minimal terdapat 1 huruf kapital dan 1 angka'

    registerAdminSistemForm = RegisterAdminSistemForm(request.POST)
    response['form'] = registerAdminSistemForm
    response['register_peran'] = 'Admin Sistem'

    return render(request, 'app1/register.html', response)

def editAdminSistem(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_dashboard)
    response = {}
    peran = 'admin'
    if (peran != request.session.get('peran')):
        raise Http404
    set_username(request, response)

    if (request.method == 'POST'):
        data = request.POST.copy()
        data['email'] = request.session.get('username')
        form = EditAdminSistemForm(data)
        if (form.is_valid()):
            stmt = '''
            UPDATE akun_pengguna
            SET password=%s
            WHERE username=%s;
            '''
            try:
                with connection.cursor() as cursor:
                    cursor.execute(stmt, [form.cleaned_data['password'], form.cleaned_data['email']])
                    connection.commit()
                    
                return redirect(url_dashboard)
            except Exception as e:
                e = str(e)
                if ('Password harus memiliki minimal satu huruf kapital dan satu angka.' in e):
                    response['error'] = 'Password Anda belum memenuhi syarat, silahkan pastikan bahwa password minimal terdapat 1 huruf kapital dan 1 angka'

    response['form'] = EditAdminSistemForm(initial = {'email': 'email: ' + request.session.get("username")})
    return render(request, 'app1/editProfil.html', response)

def registerPenggunaPublik(request):
    if (request.session.get('is_authenticated')):
        return redirect(url_dashboard)
    peran = 'pengguna_publik'
    response = {}

    if (request.method == 'POST'):
        form = RegisterPenggunaPublikForm(request.POST)
        if (form.is_valid()):
            stmt1 = '''
            INSERT INTO akun_pengguna(username, password, peran) VALUES (%s,%s,%s);
            '''
            stmt2 = '''
            INSERT INTO pengguna_publik(username, nik, nama, status, peran, nohp) VALUES (%s, %s, %s, %s, %s, %s);
            '''
            try:
                with connection.cursor() as cursor:
                    cursor.execute(stmt1, [form.cleaned_data['email'], form.cleaned_data['password'], peran])
                    connection.commit()
                with connection.cursor() as cursor:
                    cursor.execute(stmt2, [
                        form.cleaned_data['email'],
                        form.cleaned_data['nik'],
                        form.cleaned_data['nama'],
                        'AKTIF',
                        'penanggung jawab',
                        form.cleaned_data['nohp']
                    ])
                    connection.commit()
                
                request.session['username'] = form.cleaned_data['email']
                request.session['peran'] = peran
                request.session['is_authenticated'] = True
                return redirect(url_dashboard)
            except Exception as e:
                e = str(e)
                if ('already exists' in e):
                    response['error'] = 'Email telah digunakan, silakan gunakan email lain'
                elif ('Password harus memiliki minimal satu huruf kapital dan satu angka.' in e):
                    response['error'] = 'Password Anda belum memenuhi syarat, silahkan pastikan bahwa password minimal terdapat 1 huruf kapital dan 1 angka'

    registerPenggunaPublikForm = RegisterPenggunaPublikForm(request.POST)
    response['form'] = registerPenggunaPublikForm
    response['register_peran'] = 'Pengguna Publik'

    return render(request, 'app1/register.html', response)

def editPenggunaPublik(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_dashboard)
    response = {}
    peran = 'pengguna_publik'
    if (peran != request.session.get('peran')):
        raise Http404
    set_username(request, response)

    if (request.method == 'POST'):
        data = request.POST.copy()
        data['email'] = request.session.get('username')
        form = EditPenggunaPublikForm(data)
        if (form.is_valid()):
            stmt1 = '''
            UPDATE akun_pengguna
            SET password=%s
            WHERE username=%s;
            '''
            stmt2 = '''
            UPDATE pengguna_publik
            SET
                nama=%s,
                nik=%s,
                nohp=%s
            WHERE username=%s;
            '''
            try:
                with connection.cursor() as cursor:
                    cursor.execute(stmt1, [form.cleaned_data['password'], form.cleaned_data['email']])
                    connection.commit()
                with connection.cursor() as cursor:
                    cursor.execute(stmt2, [form.cleaned_data['nama'], form.cleaned_data['nik'], form.cleaned_data['nohp'], form.cleaned_data['email']])
                    connection.commit()
                return redirect(url_dashboard)
            except Exception as e:
                e = str(e)
                if ('Password harus memiliki minimal satu huruf kapital dan satu angka.' in e):
                    response['error'] = 'Password Anda belum memenuhi syarat, silahkan pastikan bahwa password minimal terdapat 1 huruf kapital dan 1 angka'

    data = {'email': 'email: ' + request.session.get("username")}
    stmt = '''
    SELECT nama, nik, nohp
    FROM pengguna_publik
    WHERE username=%s;
    '''
    try:
        with connection.cursor() as cursor:
            cursor.execute(stmt, [request.session.get('username')])
            ret = cursor.fetchone()
            data['nama'] = ret[0]
            data['nik'] = ret[1]
            data['nohp'] = ret[2]
    except:
        pass
    response['form'] = EditPenggunaPublikForm(initial=data)
    return render(request, 'app1/editProfil.html', response)

def registerDokter(request):
    if (request.session.get('is_authenticated')):
        return redirect(url_dashboard)
    peran = 'dokter'
    response = {}

    if (request.method == 'POST'):
        form = RegisterDokterForm(request.POST)
        if (form.is_valid()):
            stmt1 = '''
            INSERT INTO akun_pengguna(username, password, peran) VALUES (%s,%s,%s);
            '''
            stmt2 = '''
            INSERT INTO admin(username) VALUES (%s);
            '''
            stmt3 = '''
            INSERT INTO dokter(username, nostr, nama, nohp, gelardepan, gelarbelakang) VALUES (%s, %s, %s, %s, %s, %s);
            '''
            try:
                with connection.cursor() as cursor:
                    cursor.execute(stmt1, [form.cleaned_data['email'], form.cleaned_data['password'], peran])
                    connection.commit()
                with connection.cursor() as cursor:
                    cursor.execute(stmt2, [form.cleaned_data['email']])
                    connection.commit()
                with connection.cursor() as cursor:
                    cursor.execute(stmt3, [
                        form.cleaned_data['email'],
                        form.cleaned_data['no_str'],
                        form.cleaned_data['nama'],
                        form.cleaned_data['no_hp'],
                        form.cleaned_data['gelar_depan'],
                        form.cleaned_data['gelar_belakang'],
                    ])
                    connection.commit()
                
                request.session['username'] = form.cleaned_data['email']
                request.session['peran'] = peran
                request.session['is_authenticated'] = True
                return redirect(url_dashboard)
            except Exception as e:
                e = str(e)
                if ('already exists' in e):
                    response['error'] = 'Email telah digunakan, silakan gunakan email lain'
                elif ('Password harus memiliki minimal satu huruf kapital dan satu angka.' in e):
                    response['error'] = 'Password Anda belum memenuhi syarat, silahkan pastikan bahwa password minimal terdapat 1 huruf kapital dan 1 angka'
    registerDokterForm = RegisterDokterForm(request.POST)
    response['form'] = registerDokterForm
    response['register_peran'] = 'Dokter'

    return render(request, 'app1/register.html', response)

def editDokter(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_dashboard)
    response = {}
    peran = 'dokter'
    if (peran != request.session.get('peran')):
        raise Http404
    set_username(request, response)

    if (request.method == 'POST'):
        data = request.POST.copy()
        data['email'] = request.session.get('username')
        form = EditDokterForm(data)
        if (form.is_valid()):
            stmt1 = '''
            UPDATE akun_pengguna
            SET password=%s
            WHERE username=%s;
            '''
            stmt2 = '''
            UPDATE dokter
            SET
                nostr=%s,
                nama=%s,
                nohp=%s,
                gelardepan=%s,
                gelarbelakang=%s
            WHERE username=%s;
            '''
            try:
                with connection.cursor() as cursor:
                    cursor.execute(stmt1, [form.cleaned_data['password'], form.cleaned_data['email']])
                    connection.commit()
                with connection.cursor() as cursor:
                    cursor.execute(stmt2, [form.cleaned_data['no_str'], form.cleaned_data['nama'], form.cleaned_data['no_hp'], form.cleaned_data['gelar_depan'], form.cleaned_data['gelar_belakang'], form.cleaned_data['email']])
                    connection.commit()
                return redirect(url_dashboard)
            except Exception as e:
                e = str(e)
                if ('Password harus memiliki minimal satu huruf kapital dan satu angka.' in e):
                    response['error'] = 'Password Anda belum memenuhi syarat, silahkan pastikan bahwa password minimal terdapat 1 huruf kapital dan 1 angka'

    data = {'email': 'email: ' + request.session.get("username")}
    stmt = '''
    SELECT nostr, nama, nohp, gelardepan, gelarbelakang
    FROM dokter
    WHERE username=%s;
    '''
    try:
        with connection.cursor() as cursor:
            cursor.execute(stmt, [request.session.get('username')])
            ret = cursor.fetchone()
            data['no_str'] = ret[0]
            data['nama'] = ret[1]
            data['no_hp'] = ret[2]
            data['gelar_depan'] = ret[3]
            data['gelar_belakang'] = ret[4]
    except:
        pass
    response['form'] = EditDokterForm(data)
    return render(request, 'app1/editProfil.html', response)

def registerAdminSatgas(request):
    if (request.session.get('is_authenticated')):
        return redirect(url_dashboard)
    peran = 'admin_satgas'
    response = {}

    if (request.method == 'POST'):
        form = RegisterAdminSatgasForm(request.POST)
        if (form.is_valid()):
            stmt1 = '''
            INSERT INTO akun_pengguna(username, password, peran) VALUES (%s,%s,%s);
            '''
            stmt2 = '''
            INSERT INTO admin(username) VALUES (%s);
            '''
            stmt3 = '''
            INSERT INTO admin_satgas(username, idfaskes) VALUES (%s, %s);
            '''
            try:
                with connection.cursor() as cursor:
                    cursor.execute(stmt1, [form.cleaned_data['email'], form.cleaned_data['password'], peran])
                    connection.commit()
                with connection.cursor() as cursor:
                    cursor.execute(stmt2, [form.cleaned_data['email']])
                    connection.commit()
                with connection.cursor() as cursor:
                    if (form.cleaned_data['kode_faskes'] == ''):
                        stmt3 = '''
                        INSERT INTO admin_satgas(username) VALUES (%s);
                        '''
                        cursor.execute(stmt3, [
                            form.cleaned_data['email'],
                        ])
                    else:
                        cursor.execute(stmt3, [
                            form.cleaned_data['email'],
                            form.cleaned_data['kode_faskes'],
                        ])
                    connection.commit()
                
                request.session['username'] = form.cleaned_data['email']
                request.session['peran'] = peran
                request.session['is_authenticated'] = True
                return redirect(url_dashboard)
            except Exception as e:
                e = str(e)
                if ('already exists' in e):
                    response['error'] = 'Email telah digunakan, silakan gunakan email lain'
                elif ('Password harus memiliki minimal satu huruf kapital dan satu angka.' in e):
                    response['error'] = 'Password Anda belum memenuhi syarat, silahkan pastikan bahwa password minimal terdapat 1 huruf kapital dan 1 angka'
    registerAdminSatgasForm = RegisterAdminSatgasForm(request.POST)
    response['form'] = registerAdminSatgasForm
    response['register_peran'] = 'Admin Satgas'

    return render(request, 'app1/register.html', response)

def editAdminSatgas(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_dashboard)
    response = {}
    peran = 'admin_satgas'
    if (peran != request.session.get('peran')):
        raise Http404
    set_username(request, response)

    if (request.method == 'POST'):
        data = request.POST.copy()
        data['email'] = request.session.get('username')
        form = EditAdminSatgasForm(data)
        if (form.is_valid()):
            stmt1 = '''
            UPDATE akun_pengguna
            SET password=%s
            WHERE username=%s;
            '''
            stmt2 = '''
            UPDATE admin_satgas
            SET
                idfaskes=%s
            WHERE username=%s;
            '''
            try:
                with connection.cursor() as cursor:
                    cursor.execute(stmt1, [form.cleaned_data['password'], form.cleaned_data['email']])
                    connection.commit()
                with connection.cursor() as cursor:
                    cursor.execute(stmt2, [form.cleaned_data['kode_faskes'], form.cleaned_data['email']])
                    connection.commit()
                return redirect(url_dashboard)
            except Exception as e:
                e = str(e)
                if ('Password harus memiliki minimal satu huruf kapital dan satu angka.' in e):
                    response['error'] = 'Password Anda belum memenuhi syarat, silahkan pastikan bahwa password minimal terdapat 1 huruf kapital dan 1 angka'

    data = {'email': 'email: ' + request.session.get("username")}
    stmt = '''
    SELECT idfaskes
    FROM admin_satgas
    WHERE username=%s;
    '''
    try:
        with connection.cursor() as cursor:
            cursor.execute(stmt, [request.session.get('username')])
            ret = cursor.fetchone()
            data['kode_faskes'] = ret[0]
    except:
        pass
    response['form'] = EditAdminSatgasForm(data)
    return render(request, 'app1/editProfil.html', response)

def listPasien(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)
    stmt1 = '''
    SELECT nik, nama FROM PASIEN WHERE idpendaftar='{0}'
    '''.format(response['username'])
    try:
        user = tuple()
        with connection.cursor() as cursor:
            cursor.execute(stmt1)
            user = cursor.fetchall()
        response['pasien'] = user
    except:
        response['error'] = 'Data tidak dapat ditampilkan'
    return render(request, 'app1/listPasien.html', response)

def detailPasien(request, idPasien):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)
    stmt1 = '''
    SELECT * FROM PASIEN WHERE nik='{0}'
    '''.format(idPasien)
    if(request.method == 'GET'):
        try:
            user = tuple()
            with connection.cursor() as cursor:
                cursor.execute(stmt1, str(idPasien))
                user = cursor.fetchone()
            response['pendaftar'] = user[1]
            response['nik'] = user[0]
            response['nama'] = user[2]
            response['kjalan'] = user[3]
            response['kkelurahan'] = user[4]
            response['kkecamatan'] = user[5]
            response['kkabkot'] = user[6]
            response['kprov'] = user[7]
            response['djalan'] = user[8]
            response['dkelurahan'] = user[9]
            response['dkecamatan'] = user[10]
            response['dkabkot'] = user[11]
            response['dprov'] = user[12]
            response['notelp'] = user[13]
            response['nohp'] = user[14]
        except Exception as e:
            e = str(e)
            print(e)
            response['error'] = 'Data tidak dapat ditampilkan'
    return render(request, 'app1/detailPasien.html', response)

def daftarPasien(request):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)

    if (request.method == 'POST'):
        isian = request.POST.copy()
        isian['pendaftar'] = request.session.get('username')
        form = DaftarPasienForm(isian)
        if (form.is_valid()):
            stmt1 = '''
            INSERT INTO pasien(nik, idPendaftar, Nama, KTP_Jalan, KTP_Kelurahan, KTP_Kecamatan, KTP_KabKot, KTP_Prov, Dom_Jalan, Dom_kelurahan, Dom_Kecamatan, Dom_KabKot, Dom_Prov, NoTelp, NoHp) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);
            '''
            try:
                with connection.cursor() as cursor:
                    cursor.execute(stmt1, [form.cleaned_data['nik'], form.cleaned_data['pendaftar'], form.cleaned_data['nama'], form.cleaned_data['jalan'], form.cleaned_data['kelurahan'], form.cleaned_data['kecamatan'], form.cleaned_data['kabkot'], form.cleaned_data['provinsi'], form.cleaned_data['jalan2'], form.cleaned_data['kelurahan2'], form.cleaned_data['kecamatan2'], form.cleaned_data['kabkot2'], form.cleaned_data['provinsi2'], form.cleaned_data['no_telp'], form.cleaned_data['no_hp']])
                    connection.commit()
                return redirect(url_dashboard)
            except Exception as e:
                e = str(e)
                print(e)
                '''if ('already exists' in e):
                    response['error'] = 'Email telah digunakan, silakan gunakan email lain'
                elif ('Password harus memiliki minimal satu huruf kapital dan satu angka.' in e):
                    response['error'] = 'Password Anda belum memenuhi syarat, silahkan pastikan bahwa password minimal terdapat 1 huruf kapital dan 1 angka'
                '''
    daftarPasienForm = DaftarPasienForm(request.POST)
    daftarPasienForm.fields['pendaftar'].widget.attrs['value'] = 'Pendaftar: ' + request.session.get('username')
    response['form'] = daftarPasienForm
    return render(request, 'app1/daftarPasien.html', response)

def updatePasien(request, idPasien, namaPasien):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)
    if (request.method == 'POST'):
        isian = request.POST.copy()
        isian['pendaftar'] = request.session.get('username')
        isian['nik'] = idPasien
        isian['nama'] = namaPasien
        form = UpdatePasienForm(isian)
        if(form.is_valid()):
            stmt1 = '''
            UPDATE pasien SET KTP_Jalan =%s, KTP_Kelurahan = %s, KTP_Kecamatan = %s, KTP_KabKot = %s, KTP_Prov = %s, Dom_Jalan = %s, Dom_kelurahan = %s, Dom_Kecamatan = %s, Dom_KabKot = %s, Dom_Prov = %s, NoTelp = %s, NoHp = %s WHERE nik = %s;
            '''
            try:
                with connection.cursor() as cursor:
                    cursor.execute(stmt1, [form.cleaned_data['jalan'], form.cleaned_data['kelurahan'], form.cleaned_data['kecamatan'], form.cleaned_data['kabkot'], form.cleaned_data['provinsi'], form.cleaned_data['jalan2'], form.cleaned_data['kelurahan2'], form.cleaned_data['kecamatan2'], form.cleaned_data['kabkot2'], form.cleaned_data['provinsi2'], form.cleaned_data['no_telp'], form.cleaned_data['no_hp'], idPasien])
                    connection.commit()
                return redirect(url_dashboard)
            except Exception as e:
                e = str(e)
                print(e)
    updatePasienForm = UpdatePasienForm(request.POST)
    updatePasienForm.fields['pendaftar'].widget.attrs['value'] = 'Pendaftar: ' + request.session.get('username')
    updatePasienForm.fields['nik'].widget.attrs['value'] = 'NIK: ' + idPasien
    updatePasienForm.fields['nama'].widget.attrs['value'] = 'Nama: ' + namaPasien
    response['form'] = updatePasienForm
    return render(request, 'app1/updatePasien.html', response)

def deletePasien(request, idPasien):
    if (not(request.session.get('is_authenticated'))):
        return redirect(url_login)
    response = {}
    set_username(request, response)
    stmt1 = '''
    DELETE FROM PASIEN WHERE nik='{0}'
    '''.format(idPasien)
    if(response['peran'] == 'pengguna_publik'):
        try:
            with connection.cursor() as cursor:
                cursor.execute(stmt1)
                connection.commit()
            return redirect(url_listPasien)
        except Exception as e:
            e = str(e)
            print(e)
    return redirect(url_listPasien)
from django.urls import re_path, path
from .views import *

app_name = 'app1'

urlpatterns = [
    path('logout/', logout, name='logout'),
    path('', homepage, name='homepage'),
    path('dashboard/', dashboard, name='dashboard'),
    path('login/', login, name='login'),
    path('registerAdminSistem/', registerAdminSistem, name='registerAdminSistem'),
    path('editAdminSistem/', editAdminSistem, name='editAdminSistem'),
    path('registerPenggunaPublik/', registerPenggunaPublik, name='registerPenggunaPublik'),
    path('editPenggunaPublik/', editPenggunaPublik, name='editPenggunaPublik'),
    path('registerDokter/', registerDokter, name='registerDokter'),
    path('editDokter/', editDokter, name='editDokter'),
    path('registerAdminSatgas/', registerAdminSatgas, name='registerAdminSatgas'),
    path('editAdminSatgas/', editAdminSatgas, name='editAdminSatgas'),
    path('listPasien/', listPasien, name='listPasien'),
    path('daftarPasien/', daftarPasien, name='daftarPasien'),
    path('updatePasien/<str:idPasien>/<str:namaPasien>', updatePasien, name='updatePasien'),
    path('detailPasien/<str:idPasien>', detailPasien, name='detailPasien'),
    path('deletePasien/<str:idPasien>', deletePasien, name='deletePasien'),
]
from django import forms
from django.db import connection

class LoginForm(forms.Form):

    error_messages = {
        'required' : 'Please Type',
        'unique': 'About user with this username already exists.',
        'password_mismatch' : "Password and confirm password don't match"
    }

    email_attrs = {
        'type': 'email',
        'placeholder' : 'Email',
        'class' : 'form-control',
        'required' : True,
        'unique' : True,
        'maxlength': 50,
    }

    password_attrs = {
        'placeholder' : 'Password',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 20,
    }

    email = forms.EmailField(widget=forms.TextInput(attrs=email_attrs))
    password = forms.CharField(widget=forms.PasswordInput(attrs=password_attrs))

# class RegisterRoleForm(forms.Form):
#     NAME_CHOICES = (('Admin Sistem', 'Admin Sistem'),
#                     ('Pengguna Publik', 'Pengguna Publik'),
#                     ('Dokter', 'Dokter'),
#                     ('Admin Satgas', 'Admin Satgas'))
#     name = forms.ChoiceField(choices=NAME_CHOICES)

class RegisterAdminSistemForm(forms.Form):
    email_attrs = {
        'type': 'email',
        'placeholder' : 'Email',
        'class' : 'form-control',
        'required' : True,
        'unique' : True,
        'maxlength': 50,
    }

    password_attrs = {
        'placeholder' : 'Password',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 20,
    }

    email = forms.EmailField(widget=forms.TextInput(attrs=email_attrs))
    password = forms.CharField(widget=forms.PasswordInput(attrs=password_attrs))

class EditAdminSistemForm(forms.Form):
    email_attrs = {
        'type': 'email',
        'placeholder' : 'Email',
        'class' : 'form-control',
        'required' : True,
        'unique' : True,
        'maxlength': 50,
        'disabled': True,
    }

    password_attrs = {
        'placeholder' : 'Password Baru',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 20,
    }

    email = forms.EmailField(widget=forms.TextInput(attrs=email_attrs))
    password = forms.CharField(widget=forms.PasswordInput(attrs=password_attrs))

class RegisterPenggunaPublikForm(forms.Form):
    email_attrs = {
        'type' : 'email',
        'placeholder' : 'Email',
        'class' : 'form-control',
        'required' : True,
        'unique' : True,
        'maxlength': 50,
    }

    password_attrs = {
        'placeholder' : 'Password',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 20,
    }

    nama_attrs = {
        'placeholder' : 'Nama',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 50,
    }

    nik_attrs = {
        'placeholder' : 'NIK',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 20,
    }

    no_attrs = {
        'placeholder' : 'No. HP',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 12,
    }

    email = forms.EmailField(widget=forms.TextInput(attrs=email_attrs))
    password = forms.CharField(widget=forms.PasswordInput(attrs=password_attrs))
    nama = forms.CharField(widget=forms.TextInput(attrs=nama_attrs))
    nik = forms.CharField(widget=forms.TextInput(attrs=nik_attrs))
    nohp = forms.CharField(widget=forms.TextInput(attrs=no_attrs))

class EditPenggunaPublikForm(forms.Form):
    email_attrs = {
        'type' : 'email',
        'placeholder' : 'Email',
        'class' : 'form-control',
        'required' : True,
        'unique' : True,
        'maxlength': 50,
        'disabled': True,
    }

    password_attrs = {
        'placeholder' : 'Password (atau password baru)',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 20,
    }

    nama_attrs = {
        'placeholder' : 'Nama',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 50,
    }

    nik_attrs = {
        'placeholder' : 'NIK',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 20,
    }

    no_attrs = {
        'placeholder' : 'No. HP',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 12,
    }

    email = forms.EmailField(widget=forms.TextInput(attrs=email_attrs))
    password = forms.CharField(widget=forms.PasswordInput(attrs=password_attrs))
    nama = forms.CharField(widget=forms.TextInput(attrs=nama_attrs))
    nik = forms.CharField(widget=forms.TextInput(attrs=nik_attrs))
    nohp = forms.CharField(widget=forms.TextInput(attrs=no_attrs))

class RegisterDokterForm(forms.Form):
    email_attrs = {
        'type': 'email',
        'placeholder' : 'Email',
        'class' : 'form-control',
        'required' : True,
        'unique' : True,
        'maxlength': 50,
    }

    password_attrs = {
        'placeholder' : 'Password',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 20,
    }

    nama_attrs = {
        'placeholder' : 'Nama',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 50,
    }

    no_str_attrs = {
        'placeholder' : 'No. STR',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 20,
    }

    no_hp_attrs = {
        'placeholder' : 'No. HP',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 12,
    }

    gelar_depan_attrs = {
        'placeholder' : 'Gelar Depan',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 10,
    }

    gelar_belakang_attrs = {
        'placeholder' : 'Gelar Belakang',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 10,
    }

    email = forms.EmailField(widget=forms.TextInput(attrs=email_attrs))
    password = forms.CharField(widget=forms.PasswordInput(attrs=password_attrs))
    no_str = forms.CharField(widget=forms.TextInput(attrs=no_str_attrs))
    nama = forms.CharField(widget=forms.TextInput(attrs=nama_attrs))
    no_hp = forms.CharField(widget=forms.TextInput(attrs=no_hp_attrs))
    gelar_depan = forms.CharField(widget=forms.TextInput(attrs=gelar_depan_attrs))
    gelar_belakang = forms.CharField(widget=forms.TextInput(attrs=gelar_belakang_attrs))

class EditDokterForm(forms.Form):
    email_attrs = {
        'type': 'email',
        'placeholder' : 'Email',
        'class' : 'form-control',
        'required' : True,
        'unique' : True,
        'maxlength': 50,
        'disabled': True,
    }

    password_attrs = {
        'placeholder' : 'Password (atau password baru)',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 20,
    }

    nama_attrs = {
        'placeholder' : 'Nama',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 50,
    }

    no_str_attrs = {
        'placeholder' : 'No. STR',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 20,
    }

    no_hp_attrs = {
        'placeholder' : 'No. HP',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 12,
    }

    gelar_depan_attrs = {
        'placeholder' : 'Gelar Depan',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 10,
    }

    gelar_belakang_attrs = {
        'placeholder' : 'Gelar Belakang',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 10,
    }

    email = forms.EmailField(widget=forms.TextInput(attrs=email_attrs))
    password = forms.CharField(widget=forms.PasswordInput(attrs=password_attrs))
    no_str = forms.CharField(widget=forms.TextInput(attrs=no_str_attrs))
    nama = forms.CharField(widget=forms.TextInput(attrs=nama_attrs))
    no_hp = forms.CharField(widget=forms.TextInput(attrs=no_hp_attrs))
    gelar_depan = forms.CharField(widget=forms.TextInput(attrs=gelar_depan_attrs))
    gelar_belakang = forms.CharField(widget=forms.TextInput(attrs=gelar_belakang_attrs))

class RegisterAdminSatgasForm(forms.Form):
    email_attrs = {
        'type': 'email',
        'placeholder' : 'Email',
        'class' : 'form-control',
        'required' : True,
        'unique' : True,
        'maxlength': 50,
    }

    password_attrs = {
        'placeholder' : 'Password',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 20,
    }

    kode_faskes_attrs = {
        'placeholder' : 'Kode Faskes',
        'class' : 'form-control',
        'required' : False,
    }

    def get_kode_faskes():
        stmt = '''
        SELECT kode FROM faskes;
        '''
        kode_faskes = tuple()
        try:
            with connection.cursor() as cursor:
                cursor.execute(stmt)
                kode_faskes = cursor.fetchall()
        except:
            pass
        return [('', 'Kode Faskes')] + [(kode[0], kode[0]) for kode in kode_faskes]
        

    FASKES_CHOICES = get_kode_faskes()

    email = forms.EmailField(widget=forms.TextInput(attrs=email_attrs))
    password = forms.CharField(widget=forms.PasswordInput(attrs=password_attrs))
    kode_faskes = forms.ChoiceField(choices=FASKES_CHOICES, widget=forms.Select(attrs=kode_faskes_attrs), required=False)

class EditAdminSatgasForm(forms.Form):
    email_attrs = {
        'type': 'email',
        'placeholder' : 'Email',
        'class' : 'form-control',
        'required' : True,
        'unique' : True,
        'maxlength': 50,
        'disabled': True,
    }

    password_attrs = {
        'placeholder' : 'Password',
        'class' : 'form-control',
        'required' : True,
        'maxlength': 20,
    }

    kode_faskes_attrs = {
        'placeholder' : 'Kode Faskes',
        'class' : 'form-control',
        'required' : False,
    }

    def get_kode_faskes():
        stmt = '''
        SELECT kode FROM faskes;
        '''
        kode_faskes = tuple()
        try:
            with connection.cursor() as cursor:
                cursor.execute(stmt)
                kode_faskes = cursor.fetchall()
        except:
            pass
        return [('', 'Kode Faskes')] + [(kode[0], kode[0]) for kode in kode_faskes]
        

    FASKES_CHOICES = get_kode_faskes()

    email = forms.EmailField(widget=forms.TextInput(attrs=email_attrs))
    password = forms.CharField(widget=forms.PasswordInput(attrs=password_attrs))
    kode_faskes = forms.ChoiceField(choices=FASKES_CHOICES, widget=forms.Select(attrs=kode_faskes_attrs), required=False)

class DaftarPasienForm(forms.Form):

    pendaftar_attrs = {
        'class' : 'form-control pasien',
        'disabled' : True,
        'placeholder' : 'Pendaftar',
    }

    nik_attrs = {
        'class' : 'form-control pasien',
        'required' : True,
        'placeholder' : 'NIK',
        'maxlength': 20,
    }

    nama_attrs = {
        'class' : 'form-control pasien',
        'required' : True,
        'placeholder' : 'Nama',
        'maxlength': 50,
    }

    no_telp_attrs = {
        'class' : 'form-control pasien',
        'required' : True,
        'placeholder' : 'No. Telp',
        'maxlength': 20,
    }

    no_hp_attrs = {
        'class' : 'form-control pasien',
        'required' : True,
        'placeholder' : 'No. HP',
        'maxlength': 12,
    }

    jalan_attrs = {
        'class' : 'form-control pasien',
        'required' : True,
        'placeholder' : 'Alamat (Sesuai KTP)',
        'maxlength': 30,
    }

    kelurahan_attrs = {
        'class' : 'form-control pasien',
        'required' : True,
        'placeholder' : 'Kelurahan (Sesuai KTP)',
        'maxlength': 30,
    }

    kecamatan_attrs = {
        'class' : 'form-control pasien',
        'required' : True,
        'placeholder' : 'Kecamatan (Sesuai KTP)',
        'maxlength': 30,
    }

    kabkot_attrs = {
        'class' : 'form-control pasien',
        'required' : True,
        'placeholder' : 'Kabupaten/Kota (Sesuai KTP)',
        'maxlength': 30,
    }

    provinsi_attrs = {
        'class' : 'form-control pasien',
        'required' : True,
        'placeholder' : 'Provinsi (Sesuai KTP)',
        'maxlength': 30,
    }

    jalan2_attrs = {
        'class' : 'form-control pasien',
        'required' : True,
        'placeholder' : 'Alamat (Sesuai Domisili)',
        'maxlength': 30,
    }

    kelurahan2_attrs = {
        'class' : 'form-control pasien',
        'required' : True,
        'placeholder' : 'Kelurahan (Sesuai Domisili)',
        'maxlength': 30,
    }

    kecamatan2_attrs = {
        'class' : 'form-control pasien',
        'required' : True,
        'placeholder' : 'Kecamatan (Sesuai Domisili)',
        'maxlength': 30,
    }

    kabkot2_attrs = {
        'class' : 'form-control pasien',
        'required' : True,
        'placeholder' : 'Kabupaten/Kota (Sesuai Domisili)',
        'maxlength': 30,
    }

    provinsi2_attrs = {
        'class' : 'form-control pasien',
        'required' : True,
        'placeholder' : 'Provinsi (Sesuai Domisili)',
        'maxlength': 30,
    }

    pendaftar = forms.EmailField(widget=forms.TextInput(attrs=pendaftar_attrs))
    nik = forms.CharField(widget=forms.TextInput(attrs=nik_attrs))
    nama = forms.CharField(widget=forms.TextInput(attrs=nama_attrs))
    no_telp = forms.CharField(widget=forms.TextInput(attrs=no_telp_attrs))
    no_hp = forms.CharField(widget=forms.TextInput(attrs=no_hp_attrs))
    jalan = forms.CharField(widget=forms.TextInput(attrs=jalan_attrs))
    kelurahan = forms.CharField(widget=forms.TextInput(attrs=kelurahan_attrs))
    kecamatan = forms.CharField(widget=forms.TextInput(attrs=kecamatan_attrs))
    kabkot = forms.CharField(widget=forms.TextInput(attrs=kabkot_attrs))
    provinsi = forms.CharField(widget=forms.TextInput(attrs=provinsi_attrs))
    jalan2 = forms.CharField(widget=forms.TextInput(attrs=jalan2_attrs))
    kelurahan2 = forms.CharField(widget=forms.TextInput(attrs=kelurahan2_attrs))
    kecamatan2 = forms.CharField(widget=forms.TextInput(attrs=kecamatan2_attrs))
    kabkot2 = forms.CharField(widget=forms.TextInput(attrs=kabkot2_attrs))
    provinsi2 = forms.CharField(widget=forms.TextInput(attrs=provinsi2_attrs))

class UpdatePasienForm(forms.Form):

    pendaftar_attrs = {
        'class' : 'form-control pasien',
        'disabled' : True,
        'placeholder' : 'Pendaftar',
    }

    nik_attrs = {
        'class' : 'form-control pasien',
        'disabled' : True,
        'placeholder' : 'NIK',
    }

    nama_attrs = {
        'class' : 'form-control pasien',
        'disabled' : True,
        'placeholder' : 'Nama',
    }

    no_telp_attrs = {
        'class' : 'form-control pasien',
        'required' : True,
        'placeholder' : 'No. Telp',
        'maxlength': 20,
    }

    no_hp_attrs = {
        'class' : 'form-control pasien',
        'required' : True,
        'placeholder' : 'No. HP',
        'maxlength': 12,
    }

    jalan_attrs = {
        'class' : 'form-control pasien',
        'required' : True,
        'placeholder' : 'Alamat (Sesuai KTP)',
        'maxlength': 30,
    }

    kelurahan_attrs = {
        'class' : 'form-control pasien',
        'required' : True,
        'placeholder' : 'Kelurahan (Sesuai KTP)',
        'maxlength': 30,
    }

    kecamatan_attrs = {
        'class' : 'form-control pasien',
        'required' : True,
        'placeholder' : 'Kecamatan (Sesuai KTP)',
        'maxlength': 30,
    }

    kabkot_attrs = {
        'class' : 'form-control pasien',
        'required' : True,
        'placeholder' : 'Kabupaten/Kota (Sesuai KTP)',
        'maxlength': 30,
    }

    provinsi_attrs = {
        'class' : 'form-control pasien',
        'required' : True,
        'placeholder' : 'Provinsi (Sesuai KTP)',
        'maxlength': 30,
    }

    jalan2_attrs = {
        'class' : 'form-control pasien',
        'required' : True,
        'placeholder' : 'Alamat (Sesuai Domisili)',
        'maxlength': 30,
    }

    kelurahan2_attrs = {
        'class' : 'form-control pasien',
        'required' : True,
        'placeholder' : 'Kelurahan (Sesuai Domisili)',
        'maxlength': 30,
    }

    kecamatan2_attrs = {
        'class' : 'form-control pasien',
        'required' : True,
        'placeholder' : 'Kecamatan (Sesuai Domisili)',
        'maxlength': 30,
    }

    kabkot2_attrs = {
        'class' : 'form-control pasien',
        'required' : True,
        'placeholder' : 'Kabupaten/Kota (Sesuai Domisili)',
        'maxlength': 30,
    }

    provinsi2_attrs = {
        'class' : 'form-control pasien',
        'required' : True,
        'placeholder' : 'Provinsi (Sesuai Domisili)',
        'maxlength': 30,
    }

    pendaftar = forms.EmailField(widget=forms.TextInput(attrs=pendaftar_attrs))
    nik = forms.CharField(widget=forms.TextInput(attrs=nik_attrs))
    nama = forms.CharField(widget=forms.TextInput(attrs=nama_attrs))
    no_telp = forms.CharField(widget=forms.TextInput(attrs=no_telp_attrs))
    no_hp = forms.CharField(widget=forms.TextInput(attrs=no_hp_attrs))
    jalan = forms.CharField(widget=forms.TextInput(attrs=jalan_attrs))
    kelurahan = forms.CharField(widget=forms.TextInput(attrs=kelurahan_attrs))
    kecamatan = forms.CharField(widget=forms.TextInput(attrs=kecamatan_attrs))
    kabkot = forms.CharField(widget=forms.TextInput(attrs=kabkot_attrs))
    provinsi = forms.CharField(widget=forms.TextInput(attrs=provinsi_attrs))
    jalan2 = forms.CharField(widget=forms.TextInput(attrs=jalan2_attrs))
    kelurahan2 = forms.CharField(widget=forms.TextInput(attrs=kelurahan2_attrs))
    kecamatan2 = forms.CharField(widget=forms.TextInput(attrs=kecamatan2_attrs))
    kabkot2 = forms.CharField(widget=forms.TextInput(attrs=kabkot2_attrs))
    provinsi2 = forms.CharField(widget=forms.TextInput(attrs=provinsi2_attrs))
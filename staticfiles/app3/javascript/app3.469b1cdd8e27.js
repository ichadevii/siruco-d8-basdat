$(document).ready(function(){
    function getCookie(name) {
        let cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            const cookies = document.cookie.split(';');
            for (let i = 0; i < cookies.length; i++) {
                const cookie = cookies[i].trim();
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    var csrftoken = getCookie('csrftoken');
    
    $('select#select-kodeRS').on('change', function(){
        console.log("Select Terpilih");
        console.log( $("#select-kodeRS option:selected").val());
        kode_RS = $("#select-kodeRS option:selected").val();
        $.ajax({
            url : "/data/BasedOnKodeRS/",
            type : "POST",
            dataType: 'json',
            data : {
                'kode_RS' :  kode_RS,
            },
            headers: {
                "X-CSRFToken": csrftoken,
            },  
            success : function(data){
                console.log("sukses")
                var listKodeRuangan = data.list_kode_ruangan;
                var listKodeBed = data.list_kode_bed;
                
                $('select#select-kode-ruangan').empty();
                $('select#select-kode-bed').empty();

                for (var i=0; i<listKodeRuangan.length; i++){
                    var kodeRuangan = listKodeRuangan[i];
                    var changedHTML = '<option value=' + kodeRuangan + '>' + kodeRuangan + '</option>';

                    $('select#select-kode-ruangan').append(changedHTML);
                }

                for (var i=0; i<listKodeBed.length; i++){
                    var kodeBed = listKodeBed[i];
                    var changedHTML = '<option value=' + kodeBed + '>' + kodeBed + '</option>';

                    $('select#select-kode-bed').append(changedHTML);
                }
            }
        });
    });


    // SET VALUE ISRUJUKAN UNTUK UPDATE RUMAH SAKIT
    $("#isrujukan").on('change', function(){
        console.log($("#isrujukan-hidden").attr('value'));
        var valueSebelumnya = $("#isrujukan-hidden").attr('value');
        if (valueSebelumnya === '0'){
            $("#isrujukan")[0].checked = true;
            $("#isrujukan").val('1');
            $("#isrujukan-hidden").val('1');
        } else if (valueSebelumnya === '1'){
            // $("p#p-rujukan").empty();
            // var newvalue = '<input type="checkbox" name="rujukan" value="0" label="Rujukan" id="isrujukan"></input>'
            // $("p#p-rujukan").append(newvalue);
            $("#isrujukan")[0].checked = false;
            $("#isrujukan").val('0');
            $("#isrujukan-hidden").val('0');
        } else {
        
        }
        console.log($("#isrujukan-hidden").attr('value'));
    });

    // SET VALUE STATUS UNTUK UPDATE TRANSAKSI RUMAH SAKIT
    $("select#statusBayar").on('change', function(){
        $("#statusbayar-hidden").val($("#statusBayar").val());
        console.log($("#statusBayar").val());
    });

    // SET VALUE TANGGAL KELUAR UNTUK UPDATE RESERVASI RUMAH SAKIT
    $("input[name='tglKeluar']").on("change", function(){
        $("input[name='tglkeluar']").val($("input[name='tglKeluar']").val());
    });

    // BIAR NO NYA SERIAL
    var addSerialNumber = function () {
        $('tbody.serial tr').each(function(index) {
            $(this).find('td:nth-child(1)').html(index+1);
        });
    };

    // tglMasuk harus < tglKeluar
    $("#submit-updateReservasiRS").click(function(){
        var tglMasuk = new Date($("input[name='tglmasuk']").val());
        var tglKeluar = new Date($("input[name='tglkeluar']").val());

        if(tglMasuk > tglKeluar){
            console.log(tglMasuk);
            console.log(tglKeluar);
            alert("Tanggal Keluar Harus Lebih Besar dari Tanggal Masuk");
        }
    });

    $("#submit-ReservasiRS").click(function(){
        var tglMasuk = new Date($("input[name='tglMasuk']").val());
        var tglKeluar = new Date($("input[name='tglKeluar']").val());

        if(tglMasuk > tglKeluar){
            alert("Tanggal Keluar Harus Lebih Besar dari Tanggal Masuk");
        }
    });
    
    addSerialNumber();
});
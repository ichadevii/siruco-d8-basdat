$(document).ready(function(){
    function getCookie(name) {
        let cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            const cookies = document.cookie.split(';');
            for (let i = 0; i < cookies.length; i++) {
                const cookie = cookies[i].trim();
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    var csrftoken = getCookie('csrftoken');

    $("select#kodeRS").on('change', function(){
        kode_rs = $("#kodeRS option:selected").val();
        $.ajax({
            url: "/jsonKodeRuangan/",
            type : "POST",
            dataType: 'json',
            data: {
                'kode_rs': kode_rs,
            },
            headers: {
                "X-CSRFToken": csrftoken,
            },  
            success: function (data) { 
                var listKodeRuangan = data.data_kode_ruangan;
                $('select#kodeRuangan').empty();

                for (var i=0; i<listKodeRuangan.length; i++){
                    var kodeRuangan = listKodeRuangan[i];
                    var changedHTML = '<option value=' + kodeRuangan + '>' + kodeRuangan + '</option>';

                    $('select#kodeRuangan').append(changedHTML);
                }
            }
        });
    });

    $("select#kodeRuangan").on('change', function(){
        kode_ruangan = $("#kodeRuangan option:selected").val();
        $.ajax({
            url: "/jsonKodeRuangan/",
            type : "POST",
            dataType: 'json',
            data: {
                'kode_ruangan': kode_ruangan,
            },
            headers: {
                "X-CSRFToken": csrftoken,
            },  
            success: function (data) { 
                var kodeBed = data.data_kode_bed;
                $('#kodeBed').empty();
                $('#kodeBed').val(kodeBed[0]);
            }
        });
    });

    $("select#koders").on('change', function(){
        kode_rs = $("#koders option:selected").val();
        $.ajax({
            url: "/dataKodeRuangan/",
            type : "POST",
            dataType: 'json',
            data: {
                'koders': kode_rs,
            },
            headers: {
                "X-CSRFToken": csrftoken,
            },  
            success: function (data) { 
                var kodeRuangan = data.data_kode_ruangan;
                $('#koderuangan').empty();
                $('#koderuangan').val(kodeRuangan[0]);
                
            }
        });
    });
});
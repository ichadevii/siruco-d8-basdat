from django.db import connection


stmt = '''
SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

CREATE SCHEMA IF NOT EXISTS siruco;
SET search_path TO siruco;

CREATE OR REPLACE FUNCTION siruco.update_jumlah_bed_rs()
RETURNS trigger AS
$$
DECLARE
    jml_bed INT;

BEGIN
    SELECT JmlBed
    INTO jml_bed
    FROM RUANGAN_RS
    WHERE KodeRS = NEW.KodeRS
        AND KodeRuangan = NEW.KodeRuangan;
    
    IF (TG_OP = 'INSERT') AND (TG_TABLE_NAME = 'bed_rs') THEN
        jml_bed = jml_bed + 1;
    ELSIF (TG_OP = 'INSERT') AND (TG_TABLE_NAME = 'reservasi_rs') THEN
        jml_bed = jml_bed - 1;
    ELSE
        RAISE 'Tidak ada bed yang tersedia.';
    END IF;

    UPDATE RUANGAN_RS
    SET JmlBed = jml_bed
    WHERE KodeRS = NEW.KodeRS
        AND KodeRuangan = NEW.KodeRuangan;

    RETURN NEW;
END;
$$
LANGUAGE plpgsql;

-- function untuk dapat max id di tabel TRANSAKSI HOTEL
CREATE OR REPLACE FUNCTION siruco.get_max_id_hotel()
RETURNS INTEGER AS
$$
DECLARE
	temp_id VARCHAR(10);
	max_id INTEGER;
BEGIN
	SELECT idtransaksi FROM TRANSAKSI_HOTEL ORDER BY idtransaksi DESC LIMIT 1 INTO temp_id;
  IF temp_id != '' THEN
	    SELECT split_part(temp_id, 'TRC', 2) INTO temp_id; 	-- digunain ketika idtransaksi ada hurufnya
      SELECT CAST(temp_id AS INTEGER) INTO max_id;
      RETURN max_id;
  ELSE
      RETURN 0;
  END IF;
END;
$$
LANGUAGE plpgsql;

-- function untuk dapat max id di tabel TRANSAKSI BOOKING
CREATE OR REPLACE FUNCTION siruco.get_max_id_booking()
RETURNs INTEGER AS
$$
DECLARE
	temp_id VARCHAR(10);
	max_id INTEGER;
BEGIN
	SELECT idtransaksibooking FROM TRANSAKSI_BOOKING ORDER BY idtransaksibooking DESC LIMIT 1 INTO temp_id;
	IF temp_id != '' THEN
	    SELECT split_part(temp_id, 'TRC', 2) INTO temp_id; 	-- digunain ketika idtransaksi ada hurufnya
      SELECT CAST(temp_id AS INTEGER) INTO max_id;
	    RETURN max_id;
  ELSE
      RETURN 0;
	END IF;
END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION validasi_reservasi_hotel()
RETURNS trigger AS
$$
DECLARE
	max_id_hotel INTEGER;
	max_id_booking INTEGER;
	new_id_trc VARCHAR(10);
	new_id_booking VARCHAR(10);
	harga_hotel INTEGER;
	diffDate INTEGER;
	total_harga_hotel INTEGER;
BEGIN
IF (TG_OP = 'INSERT') THEN
	SELECT get_max_id_hotel() INTO max_id_hotel;
	SELECT get_max_id_booking() INTO max_id_booking;
	SELECT harga INTO harga_hotel FROM hotel_room hr WHERE NEW.KodeRoom = hr.KodeRoom;
	diffDate := DATE_PART('day',(NEW.tglkeluar::TIMESTAMP - NEW.tglmasuk::TIMESTAMP));

	max_id_hotel := max_id_hotel + 1;
	IF max_id_hotel < 10 THEN
    	SELECT CONCAT('TRC00', max_id_hotel) INTO new_id_trc;
	ELSIF max_id_hotel < 100 THEN
    	SELECT CONCAT('TRC0', max_id_hotel) INTO new_id_trc;
	ELSE
    	SELECT CONCAT('TRC', max_id_hotel) INTO new_id_trc;
	END IF;

	max_id_booking := max_id_booking + 1;
	IF max_id_booking < 10 THEN
    	SELECT CONCAT('TRC00', max_id_booking) INTO new_id_booking;
	ELSIF max_id_booking < 100 THEN
    	SELECT CONCAT('TRC0', max_id_booking) INTO new_id_booking;
	ELSE
    	SELECT CONCAT('TRC', max_id_booking) INTO new_id_booking;
	END IF;

	total_harga_hotel := harga_hotel * diffDate;

	INSERT INTO TRANSAKSI_HOTEL(idtransaksi, kodepasien, totalbayar, statusbayar)
	VALUES(new_id_trc,NEW.KodePasien,total_harga_hotel,'Belum Lunas');

	INSERT INTO TRANSAKSI_BOOKING(idtransaksibooking, totalbayar, kodepasien, tglmasuk)
	VALUES (new_id_booking, total_harga_hotel, NEW.KodePasien, NEW.TglMasuk);

	RETURN NEW;
ELSIF (TG_OP = 'DELETE') THEN
  DELETE FROM TRANSAKSI_HOTEL AS T
  WHERE T.KodePasien = OLD.KodePasien;
  RETURN OLD;
  END IF;
END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION siruco.get_max_id()
RETURNS INTEGER AS
$$
DECLARE
	temp_id VARCHAR(10);
	max_id INTEGER;
BEGIN
	SELECT idtransaksi FROM TRANSAKSI_RS ORDER BY idtransaksi DESC LIMIT 1 INTO temp_id;
	IF temp_id != '' 
	THEN
	SELECT split_part(temp_id, 'TRRS', 2) INTO temp_id; 	-- digunain ketika idtransaksi ada hurufnya
	SELECT CAST(temp_id AS INTEGER) INTO max_id;
	ELSE
	max_id := 0;
	END IF;
	RETURN max_id;
END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION siruco.validasi_pemeriksaan()
RETURNS trigger AS
$$
DECLARE
    MAX_PASIEN INT;
    jml_pasien INT;

BEGIN
    MAX_PASIEN = 30;

    SELECT COUNT(*)
    INTO jml_pasien
    FROM MEMERIKSA
    WHERE NoSTR = NEW.NoSTR
        AND Username_Dokter = NEW.Username_Dokter
        AND Kode_Faskes = NEW.Kode_Faskes
        AND Praktek_Shift = NEW.Praktek_Shift
        AND tgl_periksa = NEW.tgl_periksa;

    IF jml_pasien >= MAX_PASIEN THEN
        RAISE 'Jadwal dokter ini sudah penuh.';
    ELSE
        UPDATE JADWAL_DOKTER 
        set jmlpasien = jmlpasien + 1 
        WHERE nostr = NEW.NoSTR
        AND username = NEW.Username_Dokter
        AND kode_faskes = NEW.Kode_Faskes
        AND shift = NEW.Praktek_Shift
        AND tanggal = NEW.tgl_periksa;
    END IF;
    
    -- ALT SOLUTION: UPDATE kolom JmlPasien di JADWAL_DOKTER

    RETURN NEW;
END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION siruco.validate_password() RETURNS trigger
    LANGUAGE plpgsql
    AS $_$
  BEGIN
    IF (NOT (NEW.password ~ E'^(?=.*[A-Z])(?=.*[0-9]).+$')) THEN
      RAISE EXCEPTION 'Password harus memiliki minimal satu huruf kapital dan satu angka.';
    END IF;
    RETURN NEW;
  END;
$_$;

CREATE OR REPLACE FUNCTION validasi_reservasi()
RETURNS trigger AS
$$
DECLARE
	new_id varchar(10);
	max_id_num int;
	diffDate int;
BEGIN
	SELECT get_max_id() INTO max_id_num;
	diffDate := DATE_PART('day',(NEW.tglkeluar::TIMESTAMP - NEW.tglmasuk::TIMESTAMP));

	max_id_num := max_id_num + 1;
	IF max_id_num < 10 THEN
    	SELECT CONCAT('TRRS0', max_id_num) INTO new_id;
	ELSE
    	SELECT CONCAT('TRRS', max_id_num) INTO new_id;
	END IF;

	INSERT INTO TRANSAKSI_RS (idtransaksi, kodepasien, tanggalpembayaran, waktupembayaran, tglmasuk, totalbiaya, statusbayar)
	VALUES (
        	new_id,
        	NEW.kodePasien,
        	NULL,
        	NULL,
        	NEW.TglMasuk,
        	500000 * diffDate,
        	'Belum Lunas');

	RETURN NEW;
END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION siruco.when_delete_daftar_pesan() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  DECLARE
    tambahanTotalBayar INTEGER;
  BEGIN
    tambahanTotalBayar := (SELECT p.harga FROM PAKET_MAKAN p WHERE p.kodeHotel = OLD.kodeHotel AND p.kodePaket = OLD.kodePaket);
    UPDATE TRANSAKSI_MAKAN 
      SET totalBayar = totalBayar - tambahanTotalBayar
      WHERE idTransaksi = OLD.id_transaksi AND idTransaksiMakan = OLD.IdTransaksiMakan;
    RETURN NEW;
  END;
$$;

CREATE OR REPLACE FUNCTION siruco.when_insert_daftar_pesan() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  DECLARE
    tambahanTotalBayar INTEGER;
  BEGIN
    tambahanTotalBayar := (SELECT p.harga FROM PAKET_MAKAN p WHERE p.kodeHotel = NEW.kodeHotel AND p.kodePaket = NEW.kodePaket);
    UPDATE TRANSAKSI_MAKAN 
      SET totalBayar = totalBayar + tambahanTotalBayar
      WHERE idTransaksi = NEW.id_transaksi AND idTransaksiMakan = NEW.IdTransaksiMakan;
    RETURN NEW;
  END;
$$;

CREATE OR REPLACE FUNCTION siruco.when_insert_transaksi_makan() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  BEGIN
    IF (
        (NOT EXISTS (SELECT t.idTransaksi FROM TRANSAKSI_HOTEL t WHERE t.idTransaksi = NEW.idTransaksi)) OR
        (NOT EXISTS (SELECT b.idTransaksiBooking FROM TRANSAKSI_BOOKING b WHERE b.idTransaksiBooking = NEW.idTransaksi))
      ) THEN
      RAISE EXCEPTION '% tidak ditemukan di transaksi hotel dan/atau transaksi booking.',NEW.idTransaksi;
    END IF;
    NEW.TotalBayar := 0;
    
    IF ((SELECT totalBayar FROM TRANSAKSI_HOTEL WHERE idTransaksi = NEW.idTransaksi) IS NULL) THEN
      UPDATE TRANSAKSI_HOTEL SET totalBayar = 0 WHERE idTransaksi = NEW.idTransaksi;
    END IF;
    
    RETURN NEW;
  END;
$$;

CREATE OR REPLACE FUNCTION siruco.when_update_daftar_pesan() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  DECLARE
    tambahanTotalBayarSebelum INTEGER;
    tambahanTotalBayarSesudah INTEGER;
  BEGIN
    tambahanTotalBayarSebelum := (SELECT p.harga FROM PAKET_MAKAN p WHERE p.kodeHotel = OLD.kodeHotel AND p.kodePaket = OLD.kodePaket);
    tambahanTotalBayarSesudah := (SELECT p.harga FROM PAKET_MAKAN p WHERE p.kodeHotel = NEW.kodeHotel AND p.kodePaket = NEW.kodePaket);
    UPDATE TRANSAKSI_MAKAN 
      SET totalBayar = totalBayar - tambahanTotalBayarSebelum
      WHERE idTransaksi = OLD.id_transaksi AND idTransaksiMakan = OLD.IdTransaksiMakan;

    UPDATE TRANSAKSI_MAKAN
      SET totalBayar = totalBayar + tambahanTotalBayarSesudah
      WHERE idTransaksi = OLD.id_transaksi AND idTransaksiMakan = OLD.IdTransaksiMakan;
  END;
$$;

CREATE OR REPLACE FUNCTION siruco.when_update_transaksi_makan() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  BEGIN
    IF ((NEW.idTransaksi = OLD.idTransaksi) AND (NEW.idTransaksiMakan = OLD.idTransaksiMakan)) THEN
      UPDATE TRANSAKSI_HOTEL SET totalBayar = totalBayar + NEW.totalBayar - OLD.totalBayar WHERE idTransaksi = NEW.idTransaksi;
      RETURN NEW;
    END IF;
    
    IF (
        (NOT EXISTS (SELECT t.idTransaksi FROM TRANSAKSI_HOTEL t WHERE t.idTransaksi = NEW.idTransaksi)) OR
        (NOT EXISTS (SELECT b.idTransaksiBooking FROM TRANSAKSI_BOOKING b WHERE b.idTransaksiBooking = NEW.idTransaksi))
      ) THEN
      RAISE EXCEPTION '% tidak ditemukan di transaksi hotel dan/atau transaksi booking.',NEW.idTransaksi;
    END IF;
    
    UPDATE TRANSAKSI_HOTEL SET totalBayar = totalBayar - OLD.totalBayar WHERE idTransaksi = OLD.idTransaksi;
    IF ((SELECT totalBayar FROM TRANSAKSI_HOTEL WHERE idTransaksi = NEW.idTransaksi) IS NULL) THEN
      UPDATE TRANSAKSI_HOTEL SET totalBayar = 0 WHERE idTransaksi = NEW.idTransaksi;
    END IF;
    UPDATE TRANSAKSI_HOTEL SET totalBayar = totalBayar + NEW.totalBayar WHERE idTransaksi = NEW.idTransaksi;

    RETURN NEW;
  END;
$$;

CREATE TABLE IF NOT EXISTS siruco.akun_pengguna (
    username character varying(50) NOT NULL,
    password character varying(20) NOT NULL,
    peran character varying(20) NOT NULL,
    PRIMARY KEY (username)
);

CREATE TABLE IF NOT EXISTS siruco.admin (
    username character varying(50) NOT NULL,
    PRIMARY KEY (username),
    FOREIGN KEY (username) REFERENCES siruco.akun_pengguna(username) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS siruco.pengguna_publik (
    username character varying(50) NOT NULL,
    nik character varying(20) NOT NULL,
    nama character varying(50) NOT NULL,
    status character varying(12) NOT NULL,
    peran character varying(20) NOT NULL,
    nohp character varying(12) NOT NULL,
    PRIMARY KEY (username),
    FOREIGN KEY (username) REFERENCES siruco.akun_pengguna(username) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS siruco.pasien (
    nik character varying(20) NOT NULL,
    idpendaftar character varying(50) NOT NULL,
    nama character varying(50) NOT NULL,
    ktp_jalan character varying(30) NOT NULL,
    ktp_kelurahan character varying(30) NOT NULL,
    ktp_kecamatan character varying(30) NOT NULL,
    ktp_kabkot character varying(30) NOT NULL,
    ktp_prov character varying(30) NOT NULL,
    dom_jalan character varying(30) NOT NULL,
    dom_kelurahan character varying(30) NOT NULL,
    dom_kecamatan character varying(30) NOT NULL,
    dom_kabkot character varying(30) NOT NULL,
    dom_prov character varying(30) NOT NULL,
    notelp character varying(20) NOT NULL,
    nohp character varying(12) NOT NULL,
    PRIMARY KEY (nik),
    FOREIGN KEY (idpendaftar) REFERENCES siruco.pengguna_publik(username) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS siruco.komorbid_pasien (
    nik character varying(20) NOT NULL,
    namakomorbid character varying(50) NOT NULL,
    PRIMARY KEY (nik, namakomorbid),
    FOREIGN KEY (nik) REFERENCES siruco.pasien(nik) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS siruco.gejala_pasien (
    nik character varying(20) NOT NULL,
    namagejala character varying(50) NOT NULL,
    PRIMARY KEY (nik, namagejala),
    FOREIGN KEY (nik) REFERENCES siruco.pasien(nik) ON UPDATE CASCADE ON DELETE CASCADE
);


CREATE TABLE IF NOT EXISTS siruco.tes (
    nik_pasien character varying(20) NOT NULL,
    tanggaltes date NOT NULL,
    jenis character varying(10) NOT NULL,
    status character varying(15) NOT NULL,
    nilaict character varying(5),
    PRIMARY KEY (nik_pasien, tanggaltes),
    FOREIGN KEY (nik_pasien) REFERENCES siruco.pasien(nik) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS siruco.dokter (
    username character varying(50) NOT NULL,
    nostr character varying(20) NOT NULL,
    nama character varying(50) NOT NULL,
    nohp character varying(12) NOT NULL,
    gelardepan character varying(10) NOT NULL,
    gelarbelakang character varying(10) NOT NULL,
    PRIMARY KEY (username, nostr),
    FOREIGN KEY (username) REFERENCES siruco.admin(username) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS siruco.faskes (
    kode character varying(3) NOT NULL,
    tipe character varying(30) NOT NULL,
    nama character varying(50) NOT NULL,
    statusmilik character varying(30) NOT NULL,
    jalan character varying(30) NOT NULL,
    kelurahan character varying(30) NOT NULL,
    kecamatan character varying(30) NOT NULL,
    kabkot character varying(30) NOT NULL,
    prov character varying(30) NOT NULL,
    PRIMARY KEY (kode)
);

CREATE TABLE IF NOT EXISTS siruco.telepon_faskes (
    kode_faskes character varying(3) NOT NULL,
    notelp character varying(20) NOT NULL,
    PRIMARY KEY (kode_faskes, notelp),
    FOREIGN KEY (kode_faskes) REFERENCES siruco.faskes(kode) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS siruco.admin_satgas (
    username character varying(50) NOT NULL,
    idfaskes character varying(3),
    PRIMARY KEY (username),
    FOREIGN KEY (idfaskes) REFERENCES siruco.faskes(kode) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (username) REFERENCES siruco.admin(username) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS siruco.jadwal (
    kode_faskes character varying(3) NOT NULL,
    shift character varying(15) NOT NULL,
    tanggal date NOT NULL,
    PRIMARY KEY (kode_faskes, shift, tanggal),
    FOREIGN KEY (kode_faskes) REFERENCES siruco.faskes(kode) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS siruco.jadwal_dokter (
    nostr character varying(20) NOT NULL,
    username character varying(50) NOT NULL,
    kode_faskes character varying(3) NOT NULL,
    shift character varying(15) NOT NULL,
    tanggal date NOT NULL,
    jmlpasien integer,
    PRIMARY KEY (nostr, username, kode_faskes, shift, tanggal),
    FOREIGN KEY (kode_faskes, shift, tanggal) REFERENCES siruco.jadwal(kode_faskes, shift, tanggal) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS siruco.memeriksa (
    nik_pasien character varying(20) NOT NULL,
    nostr character varying(20) NOT NULL,
    username_dokter character varying(50) NOT NULL,
    kode_faskes character varying(3) NOT NULL,
    praktek_shift character varying(15) NOT NULL,
    tgl_periksa date NOT NULL,
    rekomendasi character varying(500),
    PRIMARY KEY (nik_pasien, nostr, username_dokter, kode_faskes, praktek_shift, tgl_periksa),
    FOREIGN KEY (nik_pasien) REFERENCES siruco.pasien(nik) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (nostr, username_dokter, kode_faskes, praktek_shift, tgl_periksa) REFERENCES siruco.jadwal_dokter(nostr, username, kode_faskes, shift, tanggal) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS siruco.rumah_sakit (
    kode_faskes character varying(3) NOT NULL,
    isrujukan character(1),
    PRIMARY KEY (kode_faskes),
    FOREIGN KEY (kode_faskes) REFERENCES siruco.faskes(kode) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS siruco.ruangan_rs (
    koders character varying(3) NOT NULL,
    koderuangan character varying(5) NOT NULL,
    tipe character varying(10) NOT NULL,
    jmlbed integer NOT NULL,
    harga integer NOT NULL,
    PRIMARY KEY (koders, koderuangan),
    FOREIGN KEY (koders) REFERENCES siruco.rumah_sakit(kode_faskes) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS siruco.bed_rs (
    koderuangan character varying(5) NOT NULL,
    koders character varying(3) NOT NULL,
    kodebed character varying(5) NOT NULL,
    PRIMARY KEY (koderuangan, koders, kodebed),
    FOREIGN KEY (koders, koderuangan) REFERENCES siruco.ruangan_rs(koders, koderuangan) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS siruco.reservasi_rs (
    kodepasien character varying(20) NOT NULL,
    tglmasuk date NOT NULL,
    tglkeluar date NOT NULL,
    koders character varying(3) NOT NULL,
    koderuangan character varying(5) NOT NULL,
    kodebed character varying(5) NOT NULL,
    PRIMARY KEY (kodepasien, tglmasuk),
    FOREIGN KEY (kodepasien) REFERENCES siruco.pasien(nik) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (koders, koderuangan, kodebed) REFERENCES siruco.bed_rs(koders, koderuangan, kodebed) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (kodepasien, tglmasuk) REFERENCES siruco.reservasi_rs(kodepasien, tglmasuk) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (koders) REFERENCES siruco.rumah_sakit(kode_faskes) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS siruco.transaksi_rs (
    idtransaksi character varying(10) NOT NULL,
    kodepasien character varying(20) NOT NULL,
    tanggalpembayaran date,
    waktupembayaran timestamp without time zone,
    tglmasuk date NOT NULL,
    totalbiaya integer,
    statusbayar character varying(15) NOT NULL,
    PRIMARY KEY (idtransaksi),
    FOREIGN KEY (kodepasien, tglmasuk) REFERENCES siruco.reservasi_rs(kodepasien, tglmasuk) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS siruco.hotel (
    kode character varying(5) NOT NULL,
    nama character varying(30) NOT NULL,
    isrujukan character(1) NOT NULL,
    jalan character varying(30) NOT NULL,
    kelurahan character varying(30) NOT NULL,
    kecamatan character varying(30) NOT NULL,
    kabkot character varying(30) NOT NULL,
    prov character varying(30) NOT NULL,
    PRIMARY KEY (kode)
);

CREATE TABLE IF NOT EXISTS siruco.hotel_room (
    kodehotel character varying(5) NOT NULL,
    koderoom character varying(5) NOT NULL,
    jenisbed character varying(10) NOT NULL,
    tipe character varying(10) NOT NULL,
    harga integer NOT NULL,
    PRIMARY KEY (kodehotel, koderoom),
    FOREIGN KEY (kodehotel) REFERENCES siruco.hotel(kode) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS siruco.paket_makan (
    kodehotel character varying(5) NOT NULL,
    kodepaket character varying(5) NOT NULL,
    nama character varying(20) NOT NULL,
    harga integer NOT NULL,
    PRIMARY KEY (kodehotel, kodepaket),
    FOREIGN KEY (kodehotel) REFERENCES siruco.hotel(kode) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS siruco.reservasi_hotel (
    kodepasien character varying(20) NOT NULL,
    tglmasuk date NOT NULL,
    tglkeluar date NOT NULL,
    kodehotel character varying(5) NOT NULL,
    koderoom character varying(5) NOT NULL,
    PRIMARY KEY (kodepasien, tglmasuk),
    FOREIGN KEY (kodehotel, koderoom) REFERENCES siruco.hotel_room(kodehotel, koderoom) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (kodepasien) REFERENCES siruco.pasien(nik) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS siruco.transaksi_hotel (
    idtransaksi character varying(10) NOT NULL,
    kodepasien character varying(20) NOT NULL,
    tanggalpembayaran date,
    waktupembayaran timestamp without time zone,
    totalbayar integer,
    statusbayar character varying(15) NOT NULL,
    PRIMARY KEY (idtransaksi),
    FOREIGN KEY (kodepasien) REFERENCES siruco.pasien(nik) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS siruco.transaksi_booking (
    idtransaksibooking character varying(10) NOT NULL,
    totalbayar integer,
    kodepasien character varying(20) NOT NULL,
    tglmasuk date NOT NULL,
    PRIMARY KEY (idtransaksibooking),
    FOREIGN KEY (idtransaksibooking) REFERENCES siruco.transaksi_hotel(idtransaksi) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (kodepasien, tglmasuk) REFERENCES siruco.reservasi_hotel(kodepasien, tglmasuk) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS siruco.transaksi_makan (
    idtransaksi character varying(10) NOT NULL,
    idtransaksimakan character varying(10) NOT NULL,
    totalbayar integer,
    PRIMARY KEY (idtransaksi, idtransaksimakan),
    FOREIGN KEY (idtransaksi) REFERENCES siruco.transaksi_hotel(idtransaksi) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS siruco.DAFTAR_PESAN (
  IdTransaksiMakan VARCHAR(10) NOT NULL,
  id_pesanan SERIAL NOT NULL,
  Id_transaksi VARCHAR(10),
  KodeHotel VARCHAR(5) NOT NULL,
  KodePaket VARCHAR(5) NOT NULL,
  PRIMARY KEY (IdTransaksiMakan, id_pesanan, Id_Transaksi),
  FOREIGN KEY (Id_Transaksi, IdTransaksiMakan) REFERENCES siruco.TRANSAKSI_MAKAN(IdTransaksi, IdTransaksiMakan) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY (KodeHotel, KodePaket) REFERENCES siruco.PAKET_MAKAN(KodeHotel, KodePaket)
);

CREATE TABLE IF NOT EXISTS siruco.rumah (
    koderumah character varying(5) NOT NULL,
    nama character varying(30) NOT NULL,
    isrujukan character(1) NOT NULL,
    jalan character varying(30) NOT NULL,
    kelurahan character varying(30) NOT NULL,
    kecamatan character varying(30) NOT NULL,
    kabkot character varying(30) NOT NULL,
    prov character varying(30) NOT NULL,
    CONSTRAINT rumah_isrujukan_check CHECK (((isrujukan = '1'::bpchar) OR (isrujukan = '0'::bpchar))),
    PRIMARY KEY (koderumah)
);

CREATE TABLE IF NOT EXISTS siruco.kamar_rumah (
    koderumah character varying(5) NOT NULL,
    kodekamar character varying(5) NOT NULL,
    jenisbed character varying(20) NOT NULL,
    harga integer NOT NULL,
    PRIMARY KEY (koderumah, kodekamar),
    FOREIGN KEY (koderumah) REFERENCES siruco.rumah(koderumah) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS siruco.reservasi_rumah (
    kodepasien character varying(20) NOT NULL,
    tglmasuk date NOT NULL,
    tglkeluar date NOT NULL,
    koderumah character varying(5) NOT NULL,
    kodekamar character varying(5) NOT NULL,
    PRIMARY KEY (kodepasien, tglmasuk),
    FOREIGN KEY (kodepasien) REFERENCES siruco.pasien(nik) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (koderumah, kodekamar) REFERENCES siruco.kamar_rumah(koderumah, kodekamar) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS siruco.transaksi_rumah (
    idtransaksi character varying(10) NOT NULL,
    kodepasien character varying(20) NOT NULL,
    tanggalpembayaran date,
    waktupembayaran timestamp without time zone,
    totalbayar integer,
    statusbayar character varying(15) NOT NULL,
    tglmasuk date NOT NULL,
    PRIMARY KEY (idtransaksi),
    FOREIGN KEY (kodepasien, tglmasuk) REFERENCES siruco.reservasi_rumah(kodepasien, tglmasuk) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS siruco.gedung (
    kodegedung character varying(5) NOT NULL,
    nama character varying(30) NOT NULL,
    isrujukan character(1) NOT NULL,
    jalan character varying(30) NOT NULL,
    kelurahan character varying(30) NOT NULL,
    kecamatan character varying(30) NOT NULL,
    kabkot character varying(30) NOT NULL,
    prov character varying(30) NOT NULL,
    CONSTRAINT gedung_isrujukan_check CHECK (((isrujukan = '1'::bpchar) OR (isrujukan = '0'::bpchar))),
    PRIMARY KEY (kodegedung)
);

CREATE TABLE IF NOT EXISTS siruco.ruangan_gedung (
    kodegedung character varying(5) NOT NULL,
    koderuangan character varying(5) NOT NULL,
    namaruangan character varying(20) NOT NULL,
    PRIMARY KEY (kodegedung, koderuangan),
    FOREIGN KEY (kodegedung) REFERENCES siruco.gedung(kodegedung) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS siruco.tempat_tidur_gedung (
    kodegedung character varying(5) NOT NULL,
    koderuangan character varying(5) NOT NULL,
    notempattidur character varying(5) NOT NULL,
    PRIMARY KEY (kodegedung, koderuangan, notempattidur),
    FOREIGN KEY (kodegedung, koderuangan) REFERENCES siruco.ruangan_gedung(kodegedung, koderuangan) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS siruco.reservasi_gedung (
    kodepasien character varying(20) NOT NULL,
    tglmasuk date NOT NULL,
    tglkeluar date NOT NULL,
    kodegedung character varying(5) NOT NULL,
    noruang character varying(5) NOT NULL,
    notempattidur character varying(5) NOT NULL,
    PRIMARY KEY (kodepasien, tglmasuk),
    FOREIGN KEY (kodegedung, noruang, notempattidur) REFERENCES siruco.tempat_tidur_gedung(kodegedung, koderuangan, notempattidur) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (kodepasien) REFERENCES siruco.pasien(nik) ON UPDATE CASCADE ON DELETE CASCADE
);

DROP TRIGGER IF EXISTS trigger_validasi_reservasi_hotel ON siruco.reservasi_hotel;

DROP TRIGGER IF EXISTS trigger_tambah_jumlah_bed_rs ON siruco.bed_rs;

DROP TRIGGER IF EXISTS trigger_kurangi_jumlah_bed_rs ON siruco.reservasi_rs;

DROP TRIGGER IF EXISTS trigger_validasi_pemeriksaan ON siruco.memeriksa;

DROP TRIGGER IF EXISTS trigger_validasi_reservasi ON siruco.reservasi_rs;

DROP TRIGGER IF EXISTS validate_password ON siruco.akun_pengguna;

DROP TRIGGER IF EXISTS when_delete_daftar_pesan ON siruco.daftar_pesan;

DROP TRIGGER IF EXISTS when_insert_daftar_pesan ON siruco.daftar_pesan;

DROP TRIGGER IF EXISTS when_insert_transaksi_makan ON siruco.transaksi_makan;

DROP TRIGGER IF EXISTS when_update_daftar_pesan ON siruco.daftar_pesan;

DROP TRIGGER IF EXISTS when_update_transaksi_makan ON siruco.transaksi_makan;

CREATE TRIGGER trigger_validasi_reservasi_hotel AFTER INSERT OR DELETE ON RESERVASI_HOTEL FOR EACH ROW EXECUTE PROCEDURE validasi_reservasi_hotel();

CREATE TRIGGER trigger_tambah_jumlah_bed_rs AFTER INSERT ON BED_RS FOR EACH ROW EXECUTE PROCEDURE update_jumlah_bed_rs();

CREATE TRIGGER trigger_kurangi_jumlah_bed_rs AFTER INSERT ON RESERVASI_RS FOR EACH ROW EXECUTE PROCEDURE update_jumlah_bed_rs();

CREATE TRIGGER trigger_validasi_reservasi AFTER INSERT ON siruco.RESERVASI_RS FOR EACH ROW EXECUTE PROCEDURE siruco.validasi_reservasi();

CREATE TRIGGER trigger_validasi_pemeriksaan BEFORE INSERT ON MEMERIKSA FOR EACH ROW EXECUTE PROCEDURE validasi_pemeriksaan();

CREATE TRIGGER validate_password BEFORE INSERT OR UPDATE ON siruco.akun_pengguna FOR EACH ROW EXECUTE PROCEDURE siruco.validate_password();

CREATE TRIGGER when_delete_daftar_pesan AFTER DELETE ON siruco.daftar_pesan FOR EACH ROW EXECUTE PROCEDURE siruco.when_delete_daftar_pesan();

CREATE TRIGGER when_insert_daftar_pesan AFTER INSERT ON siruco.daftar_pesan FOR EACH ROW EXECUTE PROCEDURE siruco.when_insert_daftar_pesan();

CREATE TRIGGER when_insert_transaksi_makan BEFORE INSERT ON siruco.transaksi_makan FOR EACH ROW EXECUTE PROCEDURE siruco.when_insert_transaksi_makan();

CREATE TRIGGER when_update_daftar_pesan AFTER UPDATE ON siruco.daftar_pesan FOR EACH ROW EXECUTE PROCEDURE siruco.when_update_daftar_pesan();

CREATE TRIGGER when_update_transaksi_makan BEFORE UPDATE ON siruco.transaksi_makan FOR EACH ROW EXECUTE PROCEDURE siruco.when_update_transaksi_makan();
'''

with connection.cursor() as cursor:
    cursor.execute(stmt)